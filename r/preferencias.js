/*
/* Script para el tab de preferencias
/*  //PENDIENTE LIMPIAR ESTE CODIGO
*/

$(function(){

    var myTopics = [];
    var suggestedTopics = [];
    var allTopics = [];
    var tk = null;
    var offset = parseInt($("#list_favorites .favorites_content").children(".ag_side_post").length);

    $("#topicsearch").prop("disabled", true);

    $("body").on("domChanged", function () {

        console.log("domChange") ;
        console.log("myTopics",myTopics);
        console.log("suggestedTopics",suggestedTopics);

    });

    //$(".fwp.ag_more").hide();

    function createTopicHtml(topic, topic_name, topic_title, icon="check", active=""){

        var html ='<div class="temaBTN1 '+topic_name+'" data-title='+topic_title+' data-name='+topic_name+'>';
                html += '<a href="https://www.diariolibre.com/cronologia/ver/meta/'+topic_name+'"><span>'+topic+'</span></a>';
                html += '<span class="tbtn_separador"></span>';
                html += '<button class="tbtn '+active+'"><i class="material-icons">'+icon+'</i></button>';
            html += '</div>';

        return html;
    }

    function createFavoriteHtml(data){

        var html ='<div class="ag_side_post fwp" data-id="'+data.news_id+'">';
                html +='<a href="'+data.url+'">';
                    html +='<div class="agsp_img bg_cover" style="background-image: url('+data.imagen+');"></div>';
                html +='</a>';
                html +='<div class="agsp_info">';
                    html +='<a href="'+data.url+'">';
                        html +='<h2 class="agspi_name">'+data.title+'</h2>';
                    html +='</a>';
                    html +='<a href="https://www.diariolibre.com/cronologia/ver/meta/'+data.topic_name+'" class="tagLink">'+data.topic_title+'</a>';
                    html +='<div class="post_dropdown_more">';
                        html +='<button class="pdm_button clear"><i class="material-icons">cancel</i></button>';
                    html +='</div>';
                html +='</div>';
            html +='</div>';

        return html;
    }

    function loadUserData(token){

        var settings = {
            "async": true,
            "url": BASE_URI+"/accounts?",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
            if(response.data != null){
                var authName = response.data.name;
                var authSurname = response.data.last_name;
                var fullName = authName +" "+authSurname;
               
                firebase.auth().currentUser.photoURL !== null ? $('.uh_pic.bg_cover').css('background-image','url('+firebase.auth().currentUser.photoURL+')') : $('.uh_pic.bg_cover').css('background-image','url('+getAvatar()+')');
                
                $('.hui_info b').html(fullName);

            }
            
        });

    }

    function loadUserTopics(token){
        var settings = {
            "async": true,
            "cache": false,
            "url": BASE_URI+"/topics",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            console.log(response.data.length);
            $("#myTopicsCount").html(response.data.length);
            var els = 0; 
            $.each(response.data, function( key, value ) {
                
                $("#list_topics").append(

                    createTopicHtml(unescape(value.title), value.topic_name, unescape(value.title), "check", "active")

                );

                els++;
                if(els == response.data.length){
                    $("#list_topics .lds-topics").hide();
                    
                    loadSuggestedTopics(token, response.data);

                }

            });

            if(response.data.length == 0){
                $("#list_topics .lds-topics").hide();
                $("#list_topics").append("<p>Usted no posee t&oacute;picos hasta el momento.</p>");
                loadSuggestedTopics(token, null);
            }
        });
    }

    
    function totalFavorites(token){
        $.ajax(
            {
                async: true,
                cache: false,
                url: BASE_URI+"/favorites",
                type: "GET",
                timeout: 0,
                processData: true,
                headers: {
                    "Authorization": token
                }
            }
        ).done(function (response) {
            $("#favCounter").html(response.total);
            if(parseInt(response.total) > 6) {
                $(".fwp.ag_more").show();
            }else{
                $(".fwp.ag_more").hide();
            }
        
        });
    }

    function loadUserFavorites(token, offset=0, limit){
        
        $.ajax(
            {
                async: true,
                cache: false,
                url: BASE_URI+"/favorites",
                type: "GET",
                timeout: 0,
                processData: true,
                //data: "limit=" + limit + "&offset=" + offset,
                data:{
                    limit: limit,
                    offset: offset
                },
                headers: {
                    "Authorization": token
                }
            }
        ).done(function (response) {
            console.log(response);
            console.log("offset", response.offset);
                
            var els = 0;
            var total = parseInt($("#list_favorites .favorites_content").children(".ag_side_post").length);

            if(response.offset < response.total ){
                $("#list_favorites .favorites_content").html("");
                $.each(response.data, function( key, value ) {

                    els++;

                    //if(els < response.data.length){
                        $("#list_favorites .favorites_content").last().append(
                        
                            createFavoriteHtml(value)

                        );
                    //}

                    if(els == response.data.length){
                        $("#list_favorites .favorites_content .lds-topics").hide();
                        //$(".fwp.ag_more").hide();
                        //$(".favCounter").html(response.data.length);
                    }

                });
            
            }
            if(response.data.length == 0){
                $("#list_favorites .favorites_content .lds-topics").hide();
                $("#list_favorites .favorites_content").append("<p>Usted no posee favoritos hasta el momento.</p>");
                $(".fwp.ag_more").hide();
            }
        });
    }

    function loadSuggestedTopics(token, other = null){
        var settings = {
            "async": true,
            "cache": false,
            "url": BASE_URI+"/topics/suggested",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            
            var els = 0; 

            $.each(response.data, function( key, value ) {

                if(other != null){
                    var found = $.map(other, function(val) {
                        return val.topic_name == value.topic_name ? val.topic_name: null ;
                    });

                    if(found[0] != value.topic_name){
                        $("#suggested_topics").append(
                        
                            createTopicHtml(value.title, value.topic_name, value.title, "add", "")
                        );
                    }
                }else{

                    $("#suggested_topics").append(
                        
                        createTopicHtml(value.title, value.topic_name, value.title, "add", "")
                    );
                }

                els++;
                if(els ==response.data.length){
                    $("#suggested_topics .lds-topics").hide();
                    
                }
            });

            if(response.data.length == 0){
                $("#suggested_topics  .lds-topics").hide();
                $("#suggested_topics ").append("<p>Usted no posee t&oacute;picos hasta el momento.</p>");
            }
        });
    }

    function noRecords(parent = "#list_topics"){
        var empty = $(parent).children(".temaBTN1").length;
        if(empty <= 0){
            if(parent == "#suggested_topics" || parent == "#list_topics"){
                $(parent).append("<p>Sin t&oacute;picos disponibles</p>");
            }
            $("#myTopicsCount").html(0);
        }else{
            $("#myTopicsCount").html(empty);
        }
    }
    

    function addTopic(name, title){
        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {

            $.ajax({
                    async: true,
                    url: BASE_URI+"/topics",
                    type: "POST",
                    timeout: 0,
                    cache: false,
                    data:{
                        topic_name: name,
                        title: title
                    },
                    headers: {
                        "Authorization" : token,
                        "Content-Type" : "application/x-www-form-urlencoded",
                    }
            }).done(function (response) {

                //Nada que mostrar aqui
                    
            }).fail(function(error) {

                console.log(error);
                
            });

        });
    }

    function removeTopic(topic_name){

        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {
            
            $.ajax({
                async: true,
                url: BASE_URI+"/topics",
                method: "DELETE",
                timeout: 0,
                cache: false,
                data:{
                    topic_name: topic_name
                },
                headers: {
                    Authorization: token
                }
            }).done(function (response) {
                                
            }).fail(function(error) {

                console.log(error);
                
            });

        });
        
    }

    function getAllTopicsSearch(tk){

        $.ajax({

            type: "GET",
            cache:false,
            async: true,
            url: BASE_URI+"/topics/suggested",
            dataType: "json",
            headers: {
                Authorization: tk
            }

        }).done(function (response) {
            var els = 0;
            $.each(response.data, function( key, value ) {
                
                allTopics.push({"value":value.topic_name, "label":value.title});
                els++;
            });

            if(allTopics.length == response.data.length){
                $("#topicsearch").prop("disabled", false);
            }
            
        }).fail(function(error) {

            console.log(error);	

        });

    }

    function removeFavorite(id=null){

        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {
            
            $.ajax({
                async: true,
                url: BASE_URI+"/favorites",
                method: "DELETE",
                timeout: 0,
                cache: false,
                data:{
                    news_id: id
                },
                headers: {
                    Authorization: token
                }
            }).done(function (response) {
                
                loadUserFavorites(token,offset,10);
                
            }).fail(function(error) {

                console.log(error);
                
            });

        });
        
    }


    $( "#topicsearch" ).autocomplete({
        
        source: allTopics,
        focus: function( event, ui ) {
            $( "#topicsearch" ).val( ui.item.label );
            return false;
        },
        minLength: 1,
        select: function( event, ui ) {

            console.log(ui);
            
            var exist = $("#list_topics").find('[data-name="'+ui.item.value+'"]').length;

            console.log("exist", exist);
            
            if(exist == 0){

                $("#list_topics").append(
                    createTopicHtml(ui.item.value, ui.item.label, ui.item.value, "check", "active")
                );

                addTopic(ui.item.value,ui.item.label);
            }

            $( this ).val( ui.item.label );
            return false;

        }

    });

    $(".loadmoreBTN").on("click", function(){
        
        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {
            console.log(offset);
            loadUserFavorites(token,offset,10);
        });
        
    });


    $("#list_topics").on("click",".temaBTN1 .tbtn", function(e){

        var name = $(this).parent().data('name');
        var title = $(this).parent().data('title');
        var clone_el = $(this).parent().clone();
        clone_el.children(".tbtn").toggleClass( "active" ).children().html("add");

        $("#suggested_topics p").remove();
        $("#suggested_topics").append(clone_el);
        $(this).parent().remove();
        
        removeTopic(name,title);
        
        noRecords("#list_topics");

    });


    $("#suggested_topics").on("click",".temaBTN1 .tbtn", function(e){

        var name = $(this).parent().data('name');
        var title = $(this).parent().data('title');
        var clone_el = $(this).parent().clone();
        clone_el.children(".tbtn").toggleClass( "active" ).children().html("check");

        $("#list_topics p").remove();
        $("#list_topics").append(clone_el);
        $(this).parent().remove();

        addTopic(name,title);
        
        noRecords("#list_topics");
    });

    $("#list_favorites").on("click", ".pdm_button",function(){
        var id = $(this).closest(".ag_side_post").data("id");
        $(this).closest(".ag_side_post").remove();
        removeFavorite(id);
        $("#favCounter").html(parseInt($("#favCounter").html()-1));
    });

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            user.getIdToken().then(function(token) {
                tk = token;
                console.log(user);
                if(token){ 
                    loadUserData(token);
                    loadUserTopics(token);
                    totalFavorites(token);
                    loadUserFavorites(token, 0, 3);
                    getAllTopicsSearch(token);
                }
                
            });
        }
    });

});