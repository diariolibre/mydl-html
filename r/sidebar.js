$(document).on("click", function(e){

	if(($(e.target).hasClass('shtb_btn') || $('.shtb_btn').has(e.target).length ==  1) && sidebar.isOpened){
		sidebar.close();
	}else if(($(e.target).hasClass('shtb_btn') || $('.shtb_btn').has(e.target).length ==  1) && !sidebar.isOpened){
		sidebar.open();
	}else if((!$(e.target).hasClass('shtb_btn') || $('.shtb_btn').has(e.target).length ==  0) && sidebar.isOpened){
		sidebar.close();
	}	

});


var sidebar = {
	isOpened: false,
	open: function(){

		if (sidebar.isOpened) {
			sidebar.close();
			return false;
		}

		$('.sidebar_cont').animate({
			right: '0px'
		}, 250);
		sidebar.isOpened = true;
	},
	close: function(){
		$('.sidebar_cont').animate({
			right: '-400px'
		}, 250);
		sidebar.isOpened = false;
	}
}





firebase.auth().onAuthStateChanged(function(user) {

	if(user){
		$(".sidebar_cont .sbcu_pic.bg_cover").css('background-image','url('+user.photoURL+')');
		$(".sidebar_cont .sbcu_name").html(user.displayName);
	}else{
		$(".sbcu_links.fwp").remove();
		$(".sbc_user.fwp").remove();
		$(".sbc_logout_btn.mobile").remove();
	}
});
