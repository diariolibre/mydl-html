$(function(){

    $(".sbr_btn").on("click", function(){

        $.validator.methods.email = function( value, element ) {
            return this.optional( element ) || /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}/.test( value );
        }
        $.validator.addMethod("noSpace", function (value, element) {
            return this.optional(element) || /.*\S+.*/.test(value);
        }, "No se permite introducir espacios en blanco");
        
        $("#auth").validate({
            rules: {
                email_input: {
                    required: true,
                    email: true,
                    noSpace: true
                }
            },
            messages: {
                email_input: "Por favor introduzca una cuenta de correo v&aacute;lida"
            },
            errorPlacement: function(error, element) {
                $(".alert-box.error span").html("");
                error.appendTo($(".alert-box.error").show().children("span"));   
                $(".alert-box.success").hide(100);        
            }, 
            submitHandler: function(form) {

                var auth = firebase.auth();
                auth.languageCode = 'es';

                var emailAddress = $("#email_input").val();

                auth.sendPasswordResetEmail(emailAddress).then(function() {
                    $(".alert-box.error").hide(100);
                    $(".alert-box.success").show(100);
                }).catch(function(error) {
                    console.log(error.code);
                    var values = showErrorMsg(error.code);
                    console.log(values);
                    var first = values[0];
                    var second = values[1];
                    showAlert(first, second);
                    $(".alert-box.success").hide(100);
                });
            }
        });
    });

    $("#email_input").keydown(function (e) {
        if (e.keyCode == 32) { 
          $(this).val($(this).val() + "");
          return false; 
        }
   });
})

