document.addEventListener('DOMContentLoaded', function() {


  function overlayFormRecovered(){
    $(".signupBox .sb_right").addClass("overlayFormFlex");
    $(".sbr_title.fwp").html("Por favor iniciar sessión");
    var html = '<div class="overlayForm">';
            html += '<a href="login.html" class="sbr_btn email logout">Entrar con mis credenciales</a>';
        html += '</div>';

    return html;
  }

  function overlayFormEmailVerified(){
    $(".signupBox .sb_right").addClass("overlayFormFlex");
    $(".sbr_title.fwp").html("Correo validado con éxito");
    var html = '<div class="overlayForm">';
            html += '<a href="login.html" class="sbr_btn email logout">Entrar con mis credenciales</a>';
        html += '</div>';

    return html;
  }

  console.log("DOMContentLoaded");
  $(".sbrLoginCont").show().css("margin-top","40px");

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  var mode = urlParams.get('mode');
  var actionCode = urlParams.get('oobCode');
  var continueUrl = urlParams.get('continueUrl');
  var lang = urlParams.get('lang') || 'en';
  var newPassword ;

  var auth = firebase.auth();

  
  


  switch (mode) {
    case 'resetPassword':
      $(".lds-topics").hide();
      $(".sbr_title").show();
      $(".wrapperForm").show();
      break;
    case 'verifyEmail':
      handleVerifyEmail(auth, actionCode, continueUrl, lang);
      break;
    default:
      $(".lds-topics").hide();
      $(".sbr_title").show();
      $(".wrapperForm").html('<i class="material-icons" style="font-size:100px;margin-top:10px;color:#af2b2b;">error_outline</i>').show();
      $(".alert-box-newpass").addClass('bad-code');
      $(".alert-box-newpass.bad-code").show().children("span").html("Datos incompletos para poder continuar");   
      $(".alert-box.success").hide();
  }


    function handleResetPassword(auth, actionCode, continueUrl, lang) {
     
        var accountEmail;
        
        auth.verifyPasswordResetCode(actionCode).then(function(email) {
          var accountEmail = email;
          
          if($("#repeat_new_pass").val() === $("#new_pass").val()){
            newPassword = $("#new_pass").val();
          }
    
        auth.confirmPasswordReset(actionCode, newPassword).then(function(resp) {

          $(".alert-box-newpass").hide();
          $(".wrapperForm").html(overlayFormRecovered()).css("margin-top","100px;");
          $(".sbrLoginCont").show().css("margin-top","100px");
          $(".alert-box.success").hide();

          
        }).catch(function(error) {
          var errorCode = error.code;
          var errorMessage = error.message;
          var values = showErrorMsg(errorCode);
          var first = values[0];
          var second = values[1];

          $(".alert-box-newpass").show().children("span").html(first); 
          $(".alert-box.success").hide();  
        });
      }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        var values = showErrorMsg(errorCode);
        var first = values[0];
        var second = values[1];
        $(".alert-box-newpass").addClass('bad-code-recovery');
        $(".alert-box-newpass.bad-code-recovery").show().children("span").html(first);  
        $(".alert-box.success").hide(); 
       
      });
    }

    function handleVerifyEmail(auth, actionCode, continueUrl, lang) {

      //$(".lds-topics").hide();
      $(".sbr_title").html("Verificación de Correo electrónico").show();
      
      auth.applyActionCode(actionCode).then(function(resp) {
       
        $(".lds-topics").hide();
        $(".wrapperForm").show();
        
        $(".alert-box-newpass").hide();
        $(".wrapperForm").html(overlayFormEmailVerified()).css("margin-top","60px");
        $(".alert-box.success span").html("Su correo ha sido validado con éxito");
        $(".alert-box.success").show();

        
      }).catch(function(error) {

        var errorCode = error.code;
        var errorMessage = error.message;
        var values = showErrorMsg(errorCode);
        var first = values[0];
        var second = values[1];
        
        $(".lds-topics").hide();
        $(".wrapperForm").html('<i class="material-icons" style="font-size:100px;margin-top:10px;color:#af2b2b;">error_outline</i>').show();
        $(".alert-box-newpass").addClass('bad-code');
        $(".alert-box-newpass.bad-code").show().children("span").html(first);   
        $(".alert-box.success").hide();

        
      });
    }

    $.validator.addMethod("noSpace", function (value, element) {
        return this.optional(element) || /.*\S+.*/.test(value);
    }, "No se permite introducir espacios en blanco");

    $("#resetForm").validate({
        rules: {
            new_pass: {
                noSpace: true,
                minlength: 6,
            },
            repeat_new_pass:{
                noSpace: true,
                minlength: 6,
                equalTo: "#new_pass"
            }
        },
        messages:{
            new_pass:{
                required: "El campo Nueva contraseña es requerido"
            },
            repeat_new_pass:{
                required: "El campo Repetir contarseña es requerido",
                equalTo: "Contraseña y Repetir Contraseña no coinciden"
            }
        },
        errorPlacement: function(error, element) {
            $(".alert-box-newpass span").html("");
            error.appendTo($(".alert-box-newpass").show().children("span"));   
            //console.log(error.length);
        },
        submitHandler: function(form) {
          //console.log(form);
          switch (mode) {
            case 'resetPassword':
              handleResetPassword(auth, actionCode, continueUrl, lang);
              break;
            default:
              // Error: invalid mode.
          }
        }
    });

  $("#new_pass").on('keyup change',function(){
    if($("#resetForm").valid()){
      $(".alert-box-newpass").hide();
    }
  });

  $("#repeat_new_pass").on('keyup change',function(){
      if($("#resetForm").valid()){
        $(".alert-box-newpass").hide();
      }
  });
      $("form .sbr_btn").on("click", function(e){
        if($("#resetForm").valid()){
          $(".alert-box-newpass").hide();
          $("#resetForm").submit();
        }
        e.preventDefault();
      });
  }, false);


  