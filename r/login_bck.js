
$(function(){


//TODO: LoginAuth(email, password) // esperando el correo y password a validar 
//TODO: registerAuth(email, password)  // Esperando el correo y password a crear 
//TODO: logoutAuth() para desloguear los usuario previamente logueado

$("#registerForm, #loginForm").submit(function(e) {
    e.preventDefault();
});


function loadUserData(token){
    var settings = {
        "url": BASE_URI+"/accounts?",
        "method": "GET",
        "timeout": 0,
        "headers": {
        "Authorization": token
        },
    };

    $.ajax(settings).done(function (response) {
        //console.log(response);
        
        if(response.data != null){
            var fname = response.data.name;
            var lname = response.data.last_name;
            var email = response.data.email;
            if(fname == null || lname == null || email == null){
                window.location.replace("https://herramienta.diariolibre.com/DiarioLibreTest3/signup-confirm.html");
            }else{
                window.location.replace("https://herramienta.diariolibre.com/DiarioLibreTest3/perfil-preferencias.html");
            }
        }else{
            window.location.replace("https://herramienta.diariolibre.com/DiarioLibreTest3/signup-confirm.html");
        }

        
    });
}


firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        //console.log(user);
        //console.log("logueado");
        $(".sbr_input").removeClass("error-field");
        $(".alert-box").hide(100);
        var authName = firebase.auth().currentUser.displayName;

            var emailVerified =  firebase.auth().currentUser.emailVerified;
            var providerId =  firebase.auth().currentUser.providerData[0].providerId;
            var currentUser = firebase.auth().currentUser;

            if(!emailVerified && providerId == "password"){
                console.log("no verificado");
                user.sendEmailVerification();
            }
        
            user.getIdToken().then(function(data) {
                console.log(data)
                loadUserData(data);
            });

    } else {
    // No user is signed in.
    }
});

//registerAuth("geraldop07@gmail.com", "123456");
//            LoginAuth("geraldop07@gmail.com", "123456")
//logoutAuth()


////////////////////////////////////////////////////////


//GOOGLE SIGN-IN

//1.Create an instance of the Google provider object:
var provider = new firebase.auth.GoogleAuthProvider();

//2.Optional: Specify additional OAuth 2.0 scopes that you want to request from the authentication provider. To add a scope, call addScope.
provider.addScope('https://www.googleapis.com/auth/contacts.readonly');

//3.Optional: To localize the provider's OAuth flow to the user's preferred language without explicitly passing the relevant custom OAuth parameters, update the language code on the Auth instance before starting the OAuth flow.
firebase.auth().languageCode = 'es';
    
//                firebase.auth().signInWithRedirect(provider);

function callGoogleSignIn(){
      var provider = new firebase.auth.GoogleAuthProvider();
      firebase.auth().signInWithPopup(provider).then(function(result) {
           // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
           // The signed-in user info.
           
           // ...
     }).catch(function(error) {
         // Handle Errors here.
           var errorCode = error.code;
           var errorMessage = error.message;
           // The email of the user's account used.
           var email = error.email;
           // The firebase.auth.AuthCredential type that was used.
           var credential = error.credential;
        // ...
     });
  }
        
        
//FACEBOOK SIGN-IN     

var provider2 = new firebase.auth.FacebookAuthProvider();

function facebookSignIn() {
   firebase.auth().signInWithPopup(provider2)

   .then(function(result) {
      var token = result.credential.accessToken;
      var user = result.user;

        
        //console.log("Nombre:" + authName)

        //console.log(user);


       //window.location.replace("https://herramienta.diariolibre.com/DiarioLibreTest3/signup-confirm.html");
   }).catch(function(error) {
      console.log(error.code);
      console.log(error.message);
   });
}

//            firebase.auth().signInWithRedirect(provider2);
        
        
//E-MAIL SIGN-IN

        

function showAlert(errorMsg, field) {
    $(".sbrSignupCont .sbr_input").removeClass("error");
    $(".sbrSignupCont .alert-box span").text(errorMsg);
    if (field == "email") {
        $("#auth-email").addClass("error");
    } else if (field == "password") {
        $("#auth-pass").addClass("error");
        $("#auth-pass2").addClass("error");
    } else if (field == "all") {
        $(".sbrSignupCont .sbr_input").addClass("error");
    } else {
        $(".sbrSignupCont .sbr_input").addClass("error");
    }
    $(".sbrSignupCont .alert-box").show(100);
}
        
function showAlertLogin(errorMsg, field) {
    $(".sbrLoginCont .sbr_input").removeClass("error");
    $(".sbrLoginCont .alert-box-login span").text(errorMsg);
    if (field == "email") {
        $(".sbrLoginCont #auth-email-login").addClass("error");
    } else if (field == "password") {
        $(".sbrLoginCont #auth-pass-login").addClass("error");
    } else if (field == "all") {
        $(".sbrLoginCont .sbr_input").addClass("error");
    } else {
        $(".sbrLoginCont .sbr_input").addClass("error");
    }
    $(".sbrLoginCont .alert-box-login").show(100);
}
        
        
        
function firebaseAuth(authEmail, authPass, authPass2) {
    if (authPass != "" && authPass == authPass2) {
//                        return true;
        firebase.auth().createUserWithEmailAndPassword(authEmail, authPass)
          .then(function(result) {
              
            //window.location.replace("https://herramienta.diariolibre.com/DiarioLibreTest3/signup-confirm.html");
            //return true;
          }).catch(function(error) {
            // Handle error.
            console.log("NO Funciona");
            
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(error);
//                        showErrorMsg(errorCode);
//                        console.log(errorMsg);

            var values = showErrorMsg(errorCode);
            var first = values[0];
            var second = values[1];
            
            showAlert(first, second);
            
            return false;
          });
        } else {
//                        else if (authPass == authPass2) {
            
            var first = "Las contraseñas no coinciden";
            var second = "password";
            showAlert(first, second);
            
            return false;

        }

//                    return false;

        
        
}
        
        
function firebaseLogIn(authEmail, authPass) {
    if (authPass != "" && authPass != null) {
//                        return true;
        firebase.auth().signInWithEmailAndPassword(authEmail, authPass)
          .then(function(result) {
            // result.user.tenantId should be ‘TENANT_PROJECT_ID’.
            console.log("Se logueo");
            $(".sbrLoginCont .sbr_input").removeClass("error");
            $(".sbrLoginCont .alert-box").hide();

            /*var emailVerified =  firebase.auth().currentUser.emailVerified;
            var providerId = user.providerData.providerId;
            var currentUser = firebase.auth().currentUser;

            console.log(emailVerified,providerId)

            if(!emailVerified && providerId == "password"){
                console.log("no verificado");
                result.user.sendEmailVerification();
            }*/

            //Si tiene todos los datos completos, llevarlo a MiDL o Portada
            //Si le faltan datos llevarlo a signup-confirm.html para que llene sus datos
//                        window.location.replace("https://herramienta.diariolibre.com/DiarioLibreTest3/signup-confirm.html");
            //window.location.replace("https://herramienta.diariolibre.com/DiarioLibreTest3/mi-dl.html");
            //return true;
          }).catch(function(error) {
            // Handle error.
            console.log("No se logueo");
            
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log(error);
//                        showErrorMsg(errorCode);
            var values = showErrorMsg(errorCode);
            var first = values[0];
            var second = values[1];
            
            showAlertLogin(first, second);
            console.log(first);
            console.log(second);
            
            return false;
          });
        } else {
//                        else if (authPass == authPass2) {
            
            var first = "Credenciales incorrectas";
            var second = "all";
            showAlertLogin(first, second);
            console.log(first);
            console.log(second);
            return false;

        }

//                    return false;

        
        
}
        
function emailSignIn() {

    
    
        logoutAuth();
        var authEmail = $("#auth-email").val();
        var authPass = $("#auth-pass").val();
        var authPass2 = $("#auth-pass2").val();
    
        console.log("Funciona del todo");
        firebaseAuth(authEmail, authPass, authPass2);
    

}
        
function emailLogIn() {
    
    logoutAuth();
    var authEmail = $("#auth-email-login").val();
    var authPass = $("#auth-pass-login").val();
    
    console.log("Funciona del todo");
    firebaseLogIn(authEmail, authPass);

    return ;

}

function validateRegisterForm(){
    $.validator.addMethod("noSpace", function (value, element) {
        return this.optional(element) || /.*\S+.*/.test(value);
    }, "No se permite introducir espacios en blanco");

    var validator = $("#registerForm").validate({
        rules: {
            auth_email: {
                required: true,
                email: true,
                noSpace: true
            },
            auth_pass:{
                noSpace: true,
                required: true
            },
            auth_pass2:{
                noSpace: true,
                required: true,
                equalTo: "#auth-pass"
            }
        },
        errorPlacement: function(error, element) {
              
            error.appendTo(element.next().next());
          } 

    });
    if(validator.form()){
        return true;
    }
    
}

$( "#loginForm" ).submit(function( event ) {
    emailLogIn();
    event.preventDefault();
});

$( "#registerForm" ).submit(function( event ) {

    emailSignIn();
    
    event.preventDefault();
});
    
    // Submit al presionar Enter
    $('.sbr_input').keypress(function (e) {
      if (e.which == 13 && $("#auth-email").val() != '') {
//                    	$('form#login').submit();
            emailSignIn();
            e.preventDefault();
      }
    });
    
    // Desloguear y actualizar pagina
    $(".logout-btn").click(function() {
        logoutAuth();
        location.reload(true);
    });
 });