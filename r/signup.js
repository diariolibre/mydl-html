
var loginNow = false;
firebase.auth().languageCode = 'es';

var bcProfile = '';
var bCClient = '';


var today = new Date();
var day = '' +today.getDate();

var month = '' + (today.getMonth()+1); 

if (month.length < 2) 
    month = '0' + month;
if (day.length < 2) 
    day = '0' + day;

var year = today.getFullYear();

var bc_date = [year, month, day].join('-');

console.log('bc_date',bc_date);
  
  // Is BlueConic loaded?
  if (typeof window.blueConicClient !== 'undefined' &&
      typeof window.blueConicClient.event !== 'undefined' &&
      typeof window.blueConicClient.event.subscribe !== 'undefined') {
        profile = blueConicClient.profile.getProfile();
        bcProfile = profile;
        bCClient = blueConicClient;
        var properties = ['DL_Name', 'bc_dev_login_address','DL_Nationality','OneSignal_Player_ID'];
      profile.loadValues(properties, this, function() {
        var DL_Name = profile.getValue('DL_Name');
        var bc_dev_login_address = profile.getValue('bc_dev_login_address');
        var DL_Nationality = profile.getValue('DL_Nationality');
        console.log('DL_Name:', DL_Name);
        console.log('bc_dev_login_address:', bc_dev_login_address);
        console.log('profileid:', profile.getId());
      });
      
  } else {
    // Not yet loaded; wait for the "onBlueConicLoaded" event
    window.addEventListener('onBlueConicLoaded', function () {
      // BlueConic is loaded, now we can do API things
      profile = blueConicClient.profile.getProfile();
      bcProfile = profile;
      bCClient = blueConicClient;
      var properties = ['DL_Name', 'bc_dev_login_address','DL_Nationality','OneSignal_Player_ID'];
      profile.loadValues(properties, this, function() {
        var DL_Name = profile.getValue('DL_Name');
        var bc_dev_login_address = profile.getValue('bc_dev_login_address');
        var DL_Nationality = profile.getValue('DL_Nationality');
        console.log('DL_Name:', DL_Name);
        console.log('bc_dev_login_address:', bc_dev_login_address);
        console.log('profileid:', profile.getId());
      });
    }, false);
}

function callGoogleSignIn(){
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function(result) {
        //var token = result.credential.accessToken;
        loginNow = true;
        bcProfile.setValue("date_subscription_midl", bc_date);                    
        bCClient.profile.updateProfile();
   }).catch(function(error) {
         var errorCode = error.code;
         var errorMessage = error.message;
         var email = error.email;
         var credential = error.credential;
      // ...
   });
}

function facebookSignIn() {
   var provider = new firebase.auth.FacebookAuthProvider();
   firebase.auth().signInWithPopup(provider)

   .then(function(result) {
      var token = result.credential.accessToken;
      var user = result.user;
      loginNow = true;
      bcProfile.setValue("date_subscription_midl", bc_date);                    
      bCClient.profile.updateProfile();

   }).catch(function(error) {
      console.log(error.code);
      console.log(error.message);
   });
}

$(function(){

    $("#registerForm, #loginForm").submit(function(e) {
        e.preventDefault();
    });

    var user = firebase.auth().currentUser;

    function showAlert(errorMsg, field) {
        $(".sbrSignupCont .sbr_input").removeClass("error");
        $(".alert-box span").text(errorMsg);
        if (field == "email") {
            $("#auth-email").addClass("error");
        } else if (field == "password") {
            $("#auth-pass").addClass("error");
            $("#auth-pass2").addClass("error");
        } else if (field == "all") {
            $(".sbrSignupCont .sbr_input").addClass("error");
        } else {
            $(".sbrSignupCont .sbr_input").addClass("error");
        }
        $(".alert-box").show(100);
    }
        
    function showAlertLogin(errorMsg, field) {
        $(".sbrLoginCont .sbr_input").removeClass("error");
        $(".alert-box-login span").text(errorMsg);
        if (field == "email") {
            $(".sbrLoginCont #auth-email-login").addClass("error");
        } else if (field == "password") {
            $(".sbrLoginCont #auth-pass-login").addClass("error");
        } else if (field == "all") {
            $(".sbrLoginCont .sbr_input").addClass("error");
        } else {
            $(".sbrLoginCont .sbr_input").addClass("error");
        }
        $(".alert-box-login").show(100);
    }
        
        
    function firebaseAuth(authEmail, authPass, authPass2) {
        if (authPass != "" && authPass == authPass2) {
            firebase.auth().createUserWithEmailAndPassword(authEmail, authPass)
            .then(function(result) {

                loginNow = true;

                bcProfile.setValue("date_subscription_midl", bc_date);                    
                bCClient.profile.updateProfile();
           
            }).catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(error);
                var values = showErrorMsg(errorCode);
                var first = values[0];
                var second = values[1];
                
                showAlert(first, second);
                
                return false;
            });
        } else {
            var first = "Las contraseñas no coinciden";
            var second = "password";
            showAlert(first, second);
            
            return false;
        }    
    }
        
        
    function firebaseLogIn(authEmail, authPass) {
        if (authEmail != "" || authPass != "") {
            firebase.auth().signInWithEmailAndPassword(authEmail, authPass)
            .then(function(result) {
                console.log("Se logueo");
                $(".sbrLoginCont .sbr_input").removeClass("error");
                $(".sbrLoginCont .alert-box").hide();

                loginNow = true;

            }).catch(function(error) {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(error);
                var values = showErrorMsg(errorCode);
                var first = values[0];
                var second = values[1];
                
                showAlertLogin(first, second);
                console.log(first);
                console.log(second);
                return false;
            });
        } else {
                
            var first = "Debe ingresar correo y contraseña";
            var second = "all";
            showAlertLogin(first, second);
            console.log(first);
            console.log(second);
            return false;

        }     
    }

    $("#auth-email").on('keyup change',function(){
        var value = $(this).val();
        var domain = value.substr(value.indexOf("@") + 1);
        var tld = domain.split(".").pop();

        if(tld.length > 1){
            $(".alert-box").hide();
        }
    });

    $("#auth-email").keydown(function (e) {
        if (e.keyCode == 32) { 
          $(this).val($(this).val() + "");
          return false; 
        }
   });

    $("#auth-pass-login").on('keyup change',function(){

        if($(this).val().length >= 6){
            $(".alert-box").hide();
        }
    });

    $.validator.addMethod("noSpace", function (value, element) {
        return this.optional(element) || /.*\S+.*/.test(value);
    }, "No se permite introducir espacios en blanco");

    $.validator.methods.email = function( value, element ) {
        return this.optional( element ) || /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}/.test( value );
    }
    
    var validator = $("#registerForm").validate({
        rules: {
            auth_email: {
                required: true,
                email: true,
                noSpace: true
            },
            auth_pass:{
                noSpace: true,
                required: true,
                minlength: 6
            },
            auth_pass2:{
                noSpace: true,
                required: true,
                minlength: 6,
                equalTo: "#auth-pass"
            },
        },
        messages:{
            auth_email:{
                auth_email_login: "Introduzca un correo electrónico válido",
                required: "El campo Correo electrónico es requerido"
            },
            auth_pass:{
                required: "El campo Contraseña es requerido"
            },
            auth_pass2:{
                required: "El campo Contraseña es requerido",
                equalTo: "Contraseña y Repetir Contraseña no coinciden"
            }
        },
        errorPlacement: function(error, element) {
            $(".alert-box span").html("");
            error.appendTo($(".alert-box").show().children("span"));           
        } 

    });
        
    function emailSignIn() {
        
        logoutAuth();
        var authEmail = $("#auth-email").val();
        var authPass = $("#auth-pass").val();
        var authPass2 = $("#auth-pass2").val();
        firebaseAuth(authEmail, authPass, authPass2);

    }

    $( "#registerForm" ).submit(function( event ) {
        if($( "#registerForm" ).valid()) {
            emailSignIn();
        }
        event.preventDefault();
    });
        
    $('.sbr_input').keypress(function (e) {
      if (e.which == 13 && $("#auth-email").val() != '') {
        emailSignIn();
        e.preventDefault();
      }
      
    });
    
    $(".logout-btn").click(function() {
        logoutAuth();
        location.reload(true);
    });

    function loadUserData(token){
        var settings = {
            "url": BASE_URI+"/accounts?",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            
            if(response.data != null){
                var fname = response.data.name;
                var lname = response.data.last_name;
                var email = response.data.email;
                if(fname == null || lname == null || firebase.auth().currentUser.email == null){
                    window.location.replace(BASE_SITE+"/signup-confirm.html");
                }else{
                    window.location.replace(BASE_SITE+"/perfil-informacion.html");
                }
            }else{
                window.location.replace(BASE_SITE+"/signup-confirm.html");
            }

            
        });
    }

    $(".sbrSignupCont").on("click",".overlayForm .logout", function(){
        logoutAuth();
        location.reload(true);
    });

    function overlayForm(){
        $(".signupBox .sb_right").addClass("overlayFormFlex");
        $(".sbr_title.fwp").html("Usted ya se encuentra logueado");
        var html = '<div class="overlayForm">';
                html += '<a href="#" class="sbr_btn email logout">Entrar con otras credenciales</a>';
                html += '<a href="mi-dl.html" class="sbr_btn email">Ir a MiDL</a>';
            html += '</div>';

        return html;
    }

    firebase.auth().onAuthStateChanged(function(user) {
        $(".fwp.sbrSignupCont").show();
        $(".sbr_title").show();
        if (user) {
            $(".sbr_input").removeClass("error-field");
            $(".alert-box").hide(100);

            var emailVerified =  firebase.auth().currentUser.emailVerified;
          
            if(!emailVerified){
                user.sendEmailVerification();
            }
        
            if(loginNow){
                user.getIdToken().then(function(data) {
                    loadUserData(data);
                });
            }else{
                $(".fwp.sbrSignupCont").html(overlayForm());
            }

        }
    });

    
 });