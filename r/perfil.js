const googleProvider = new firebase.auth.GoogleAuthProvider();
const facebookProvider = new firebase.auth.FacebookAuthProvider();
const app_id = '512ce692-9589-4514-a679-ddc1585400f5';

var today = new Date();
var day = '' +today.getDate();

var month = '' + (today.getMonth()+1); 

if (month.length < 2) 
    month = '0' + month;
if (day.length < 2) 
    day = '0' + day;

var year = today.getFullYear();

var bc_date = [year, month, day].join('-');

function linkGoogle() {                
        
    firebase.auth().currentUser.linkWithPopup(googleProvider).then(function(result) {
        // Accounts successfully linked.
        var credential = result.credential;
        var user = result.user;
        linkedAccount(firebase.auth().currentUser);
        window.location.reload();
    }).catch(function(error) {
        
        if(error.code == "auth/credential-already-in-use"){
            $(".alert-box").show(100).children("span").html("Esta cuenta ya se encuetra asociada a otro usuario.");
        }
        
    });
}

function linkFacebook() {
    
    firebase.auth().currentUser.linkWithPopup(facebookProvider).then(function(result) {
        // Accounts successfully linked.
        var credential = result.credential;
        var user = result.user;
        linkedAccount(firebase.auth().currentUser);
        window.location.reload();
    }).catch(function(error) {
        if(error.code == "auth/credential-already-in-use"){
            $(".alert-box").show(100).children("span").html("Esta cuenta ya se encuetra asociada a otro usuario.");
        }

    });
}

function unLinkAccount(providerId) {

    firebase.auth().currentUser.unlink(providerId).then(function() {
        linkedAccount(firebase.auth().currentUser);

        if(providerId == "google.com"){
            $(".sbr_btn.gl span").html("Google");
            $(".sbr_btn.gl").attr("onclick","linkGoogle()");
        }
        if(providerId == "facebook.com"){
            $(".sbr_btn.fb span").html("Facebook");
            $(".sbr_btn.fb").attr("onclick","linkFacebook()");
        }

        window.location.reload();
        
    }).catch(function(error) {
    
        alert(error);
    });
    
}

function linkedAccount(user){
    var providerData = user.providerData;
    
    if(providerData.length > 0){

        const google = providerData.find( ({ providerId }) => providerId === 'google.com' );
        const facebook = providerData.find( ({ providerId }) => providerId === 'facebook.com' );

        if(typeof google !== "undefined"){
            $(".sbr_btn.gl span").html("Desvincular");
            $(".sbr_btn.gl").attr("onclick","unLinkAccount('google.com')");
        }
        if(typeof facebook !== "undefined"){
            $(".sbr_btn.fb span").html("Desvincular");
            $(".sbr_btn.fb").attr("onclick","unLinkAccount('facebook.com')");
        }

    }else{
        $(".sbr_btn.gl span").html("Google");
        $(".sbr_btn.gl").attr("onclick","linkGoogle()");
        $(".sbr_btn.fb span").html("Facebook");
        $(".sbr_btn.fb").attr("onclick","linkFacebook()");
    }

}

var bcProfile = '';
var bCClient = '';
var oneSignalId = '';

console.log(new Date().toLocaleDateString());
  
  // Is BlueConic loaded?
  if (typeof window.blueConicClient !== 'undefined' &&
      typeof window.blueConicClient.event !== 'undefined' &&
      typeof window.blueConicClient.event.subscribe !== 'undefined') {
      profile = blueConicClient.profile.getProfile();
      bcProfile = profile;
      bCClient = blueConicClient;
      var properties = ['DL_Name', 'bc_dev_login_address','DL_Nationality','OneSignal_Player_ID','date_subscription_midl','date_update_midl'];
      profile.loadValues(properties, this, function() {
        var DL_Name = profile.getValue('DL_Name');
        var bc_dev_login_address = profile.getValue('bc_dev_login_address');
        var DL_Nationality = profile.getValue('DL_Nationality');
        console.log('DL_Name:', DL_Name);
        console.log('bc_dev_login_address:', bc_dev_login_address);
        console.log('DL_Nationality:', DL_Nationality);
        console.log('OneSignal_Player_ID',profile.getValue('OneSignal_Player_ID'));
        console.log('date_subscription_midl',profile.getValue('date_subscription_midl'));
        console.log('date_update_midl',profile.getValue('date_update_midl'));
      });
  } else {
    // Not yet loaded; wait for the "onBlueConicLoaded" event
    window.addEventListener('onBlueConicLoaded', function () {
      // BlueConic is loaded, now we can do API things
      profile = blueConicClient.profile.getProfile();
      bcProfile = profile;
      bCClient = blueConicClient;

      var properties = ['DL_Name', 'bc_dev_login_address','DL_Nationality','OneSignal_Player_ID','date_subscription_midl','date_update_midl'];
      profile.loadValues(properties, this, function() {
        // Values have been loaded, safe to get now
        var DL_Name = profile.getValue('DL_Name');
        var bc_dev_login_address = profile.getValue('bc_dev_login_address');
        var DL_Nationality = profile.getValue('DL_Nationality');
        console.log('DL_Name:', DL_Name);
        console.log('bc_dev_login_address:', bc_dev_login_address);
        console.log('DL_Nationality:', DL_Nationality);
        console.log('OneSignal_Player_ID',profile.getValue('OneSignal_Player_ID'));
        console.log('date_subscription_midl',profile.getValue('date_subscription_midl'));
        console.log('date_update_midl',profile.getValue('date_update_midl'));
      });
    }, false);
}




$(function(){

    //var bcProfile = bcProfile;
    var myTopics = [];
    var suggestedTopics = [];
    var allTopics = [];
    var tk = null;
    var offset = parseInt($("#list_favorites .favorites_content").children(".ag_side_post").length);

    $("#topicsearch").prop("disabled", true);

    var tmp_avatar = getAvatar();

    function createTopicHtml(topic, topic_name, topic_title, icon="check", active=""){

        var html ='<div class="temaBTN1 '+topic_name+'" data-title='+topic_title+' data-name='+topic_name+'>';
                html += '<a href="https://www.diariolibre.com/cronologia/ver/meta/'+topic_name+'"><span>'+topic+'</span></a>';
                html += '<span class="tbtn_separador"></span>';
                html += '<button class="tbtn '+active+'"><i class="material-icons">'+icon+'</i></button>';
            html += '</div>';

        return html;
    }

    function createFavoriteHtml(data){

        var html ='<div class="ag_side_post fwp" data-id="'+data.news_id+'">';
                html +='<a href="'+data.url+'">';
                    html +='<div class="agsp_img" style="background-image: url('+data.imagen+');"></div>';
                html +='</a>';
                html +='<div class="agsp_info">';
                    html +='<a href="'+data.url+'">';
                        html +='<h2 class="agspi_name">'+data.title+'</h2>';
                    html +='</a>';
                    html +='<a href="https://www.diariolibre.com/cronologia/ver/meta/'+data.topic_name+'" class="tagLink">'+data.topic_title+'</a>';
                    html +='<div class="post_dropdown_more">';
                        html +='<button class="pdm_button clear"><i class="material-icons">cancel</i></button>';
                    html +='</div>';
                html +='</div>';
            html +='</div>';

        return html;
    }

    function createNewsletterHtml(data, icon="add", text="Suscribir"){
        var active = '';
        var id = '';
        if(icon == 'check'){
            active = 'active';
        }

        if(data.slug){
            id = data.slug
        }else{
            id = data.id;
        }



        var html ='<div class="col-md-4 col-sm-4 col-xs-12" data-order="'+data.orden+'">';
            html += '<div class="nb_item fwp '+id+' fwp '+active+'">';
                html += '<span>'+data.name+'</span> <button data-title="'+data.name+'" data-tag="'+id+'"><i class="material-icons">'+icon+'</i> <span>'+text+'</span></button>';
            html+='</div></div>';
        return html;
    }

    function createNofiticationHtml(data, icon="add", text="Suscribir"){
        var active = '';
        if(icon == 'check'){
            active = 'active';
        }

        var html ='<div order='+data.orden+'>';
                html += '<div class="nb_item '+data.slug+' fwp '+active+'">';
                    html += '<span>'+data.name+'</span> <button data-title="'+data.name+'" data-tag="'+data.slug+'"><i class="material-icons">'+icon+'</i> <span>'+text+'</span></button>';
            html += '</div></div>';

        return html;
    }

    function loadCountries(token){
        var settings = {
            "async": true,
            "url": BASE_URI+"/country/list",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            
            $.each(response.data, function(key,value){
                $("#countries").append('<option value="'+value.country_code.toUpperCase()+'">'+value.country_name+'</option>');
            });
        });

    }

    function loadUserData(token){

        var settings = {
            "async": true,
            "url": BASE_URI+"/accounts?",
            "method": "GET",
            "timeout": 0,
            "headers": {
                "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {

            console.log(response);

            var authSurname = '';
            var authName = '';
            
            if(response.hasOwnProperty('data') && response.data.hasOwnProperty('is_complete') && response.data.is_complete == true && firebase.auth().currentUser.email != null){
                
                var gender = response.data.gender;
                var birth_day = response.data.birth_day;
                var country  = response.data.country;
                authSurname = response.data.last_name;
                authName = response.data.name;

                $("#auth-surname").val(authSurname);

                $("#countries").children("option[value='"+country+"']").attr('selected', true);

                $("input[name='date']").val(birth_day);
                $("select[name='genero']").val(gender);

                if(firebase.auth().currentUser.email == null || response.data.email == ''){
                    $("#changes .ucbrc_pack").show();
                    $("#auth-email").attr("disabled", false);
                }
            }else{
                window.location.replace("signup-confirm.html");
            }


            var authEmail = firebase.auth().currentUser.email;
            //var authName = firebase.auth().currentUser.displayName;

            $('.hui_info b').html(authName + " " + authSurname);
            $("#auth-email").val(authEmail);
            $("#auth-name").val(authName);

            if(firebase.auth().currentUser.providerData.length > 1){
                
                $.each(firebase.auth().currentUser.providerData, function(index, value){

                        switch(value.providerId){
                            case "facebook.com":
                                $('.bg_cover').css('background-image','url('+value.photoURL+')');
                                break;
                            case "google.com":
                                $('.bg_cover').css('background-image','url('+value.photoURL+')');
                                break;
                        }
                    
                });

            }else{

                firebase.auth().currentUser.photoURL !== null ? 
                    $('.bg_cover').css('background-image','url('+firebase.auth().currentUser.photoURL+')') : 
                    $('.bg_cover').css('background-image','url('+tmp_avatar+')');

            }
        });

    }

    //function ValidateForm() {
        //console.log("validar form?");
        /*var authName = $("#auth-name").val();
        var authSurname = $("#auth-surname").val();
        
        var authEmailService = firebase.auth().currentUser.email;
        var authPhoto = firebase.auth().currentUser.photoURL;*/

        $.validator.addMethod("dateFormat", function(value,element) {
            return value.match(/^(0[1-9]|1[012])[- //.](0[1-9]|[12][0-9]|3[01])[- //.](19|20)\d\d$/);
        }, "Please enter a date in the format mm/dd/yyyy");

        $.validator.addMethod("noSpace", function (value, element) {
            return this.optional(element) || /.*\S+.*/.test(value);
        }, "No se permite introducir espacios en blanco");

        $.validator.methods.email = function( value, element ) {
            return this.optional( element ) || /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}/.test( value );
        }

        var validator = $("#changes").validate({
            rules: {
                auth_email:{
                    required: true,
                    noSpace: true,
                    email: true,
                },
                auth_name: {
                    required: true,
                    noSpace: true
                },
                auth_surname: {
                    required: true,
                    noSpace: true
                },
            },
            messages:{
                date:"Por favor introduzca una fecha válida",
                auth_email:{
                    email: "Introduzca un correo electrónico válido",
                    required: "El campo Correo electrónico es requerido"
                },
                auth_name:{
                    required:"El campo Nombre es requerido"
                },
                auth_surname:{
                    required:"El campo Apellidos es requerido"
                }
            },
            errorPlacement: function(error, element) {
                $(".alert-box span").html("");
                error.appendTo($(".alert-box").show().children("span"));           
            } 

        });

    function showError(error) {
        $(".alert-box span").text(error);
        $(".sbr_input").addClass("error");
        $(".alert-box").show(100);
    }

    function register() {
        console.log("resgister()");
        firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
            sendRegisterWithToken(idToken);

        }).catch(function(error) {
            
        });
    }

    function sendRegisterWithToken(token) {

        var authName = $("#auth-name").val();
        var authSurname = $("#auth-surname").val();
        var country  = $("select[name='pais']").children("option:selected").val();
        var birthDay = $("input[name='date']").val();
        var gener = $("select[name='genero']").children("option:selected").val();
        var authPhoto = null;
        var bc_profile = bcProfile;

        if(firebase.auth().currentUser.providerData.length > 1){

            $.each(firebase.auth().currentUser.providerData, function(index, value){
                if(value.providerId !== "password"){

                    authPhoto = value.photoURL !== null ? value.photoURL : tmp_avatar;

                }
            })

        }else{

            authPhoto = firebase.auth().currentUser.photoURL == null ? tmp_avatar : firebase.auth().currentUser.photoURL;

        }

        var authEmailService = firebase.auth().currentUser.email == null ? $("#auth-email").val() : firebase.auth().currentUser.email;

        
        
        /*var validate = ValidateForm();
        if(validate == true) {*/
            var settings = {
                    "url": BASE_URI+"/accounts",
                    "method": "POST",
                    "timeout": 0,
                    "cache": true,
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Authorization": token
                    },
                    "data": {
                        "name": authName,
                        "last_name": authSurname,
                        "email": authEmailService,
                        //"avatar": authPhoto,
                        "country": country,
                        "birth_day": birthDay,
                        "gender":gener,
                        "is_complete": true,
                        "bc_profileid": bcProfile.getId(),
                        "onesignal_playerid": profile.getValue('OneSignal_Player_ID'),
                    }
            };
            $.ajax(settings).done(function (response) {
                bc_profile.setValue("bc_dev_login_address", authEmailService);
                bc_profile.setValue("DL_Name", authName);
                bc_profile.setValue("DL_Last_Name", authSurname);
                bc_profile.setValue("DL_Nationality", country);
                bc_profile.setValue("BC_Dev_BirthDate", birthDay);
                bc_profile.setValue("DL_Gender", gener);
                bc_profile.setValue("date_update_midl", bc_date);
                
                blueConicClient.profile.updateProfile(this, function() {
                    console.log("BC Updated");
                });

                firebase.auth().currentUser.updateProfile({
                    photoURL: authPhoto,
                    displayName: authName
                }).then(function() {
                    // Update successful.
                }).catch(function(error) {
                    console.log(error);
                });

                $(".ucbrc_submit .lds-topics").show();
                $(".ucbrc_submit").html("Cambios guardados").removeAttr("disabled");;
                
            }) .fail(function(error) {
                var errorMsg = error.responseJSON.message;
                showError(errorMsg);
                $(".alert-box").show(100);
                $(".ucbrc_submit").html("Cambios guardados").removeAttr("disabled");;
            });
        /*} else {
            return false;
        }*/
    }

    $("#auth-name").on("change", function(){

        if($("#auth-surname").val() !="" && $(this).val() !=""){
            $(".alert-box").hide();
        }
    });

    $("#auth-surname").on('keyup change',function(){

        if($("#auth-name").val() !="" && $(this).val() !=""){
            $(".alert-box").hide();
        }
        
    });

    if($("#changes").valid()){
        $(".alert-box").hide();
    }

    $(".ucbrc_submit").on("click", function(e){
        if($("#changes").valid()){
            $(this).html("<img src='r/img/loading.gif'>").attr("disabled", 'disabled');
            console.log("valid?");
            register();
        }
        console.log("??");
        e.preventDefault();
    });

    $("#changes").submit(function(e) {
        e.preventDefault();
    });

    function logout(){
        firebase.auth().signOut().then(function() {
            window.location.replace("login.html");
        }, function(error) {
        });
    }

    function unLink() {
        var provider = new firebase.auth.EmailAuthProvider();
        firebase.auth().currentUser.unlink(provider.providerId).then(function() {
            
        }).catch(function(error) {
            
        });
    }

    function loadUserTopics(token){
        var offset = $('#list_topics').children('.temaBTN1').length;
        console.log('offset',offset);
        var settings = {
            "async": true,
            "cache": false,
            "url": BASE_URI+"/topics",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            
            $("#myTopicsCount").html(response.data.length);
            var els = 0; 
            $.each(response.data, function( key, value ) {
                
                $("#list_topics").append(

                    createTopicHtml(unescape(value.title), value.topic_name, unescape(value.title), "check", "active")

                );

                els++;
                if(els == response.data.length){
                    $("#list_topics .lds-topics").hide();
                    
                    loadSuggestedTopics(token, response.data);

                }

            });

            if(response.data.length == 0){
                $("#list_topics .lds-topics").hide();
                $("#list_topics").append("<p>Usted no posee t&oacute;picos hasta el momento.</p>");
                loadSuggestedTopics(token, null);
            }
        });
    }

    
    function totalFavorites(token){
        $.ajax(
            {
                async: true,
                cache: false,
                url: BASE_URI+"/favorites",
                type: "GET",
                timeout: 0,
                processData: true,
                headers: {
                    "Authorization": token
                }
            }
        ).done(function (response) {
            $("#favCounter").html(response.total);
            if(parseInt(response.total) > 6) {
                $(".fwp.ag_more").show();
            }else{
                $(".fwp.ag_more").hide();
            }
        
        });
    }

    function loadNewsLetters(token, other=null){
        $.ajax(
            {
                async: true,
                cache: false,
                url: "https://newsletter.diariolibre.com/v1/public/api/subscribe/lists",
                type: "GET",
                timeout: 0,
                processData: true,
                
            }
        ).done(function (response) {

            $("#nbItemsNewsletter .lds-topics").show();
            var els = 0;
            $.each(response, function( key, value ) {

                if(other != null){
                    var found = $.map(other.data, function(val) {
                        return val.list_id == value.id ? val.list_id: null ;
                    });

                    if(found[0] != value.id){
                        $("#nbItemsNewsletter").append(
                            createNewsletterHtml(value,"add","Suscribir")
                        );
        
                    }else{
                        $("#nbItemsNewsletter").append(
                            createNewsletterHtml(value,"check","Suscrito")
                        );
                    }
                }else{
                    $("#nbItemsNewsletter").append(
                        createNewsletterHtml(value,"add","Suscribir")
                    );
                    
                }

                els++;
               

                if(els == response.length){
                    $("#nbItemsNewsletter .lds-topics").hide();
                
                }
            });
            
        }).fail(function(error) {
            console.log(error);
        });
    }


    function loadMyNewsLetters(token){
        $.ajax(
            {
                async: true,
                cache: false,
                url: BASE_URI+"/newsletter",
                type: "GET",
                timeout: 0,
                processData: true,
                headers: {
                    "Authorization": token
                }
                
            }
        ).done(function (response) {
        
            loadNewsLetters(token,response);
            
        }).fail(function(error) {
            console.log(error);
        });
    }


    function subscribePush(email, list, el, token){
        $.ajax({
            type: "POST",
            url:  BASE_URI+"/newsletter",
            data: {email: email, list_id:list},
            headers: {
                "Authorization": token
            }	
        })
        .done(function(data){

            el.parent().addClass("active");
            el.children("i").html('check');
            

        })
        .fail(function(jqXHR, textStatus){		
            
            console.log(jqXHR);
            
        });
    }

    function removeNewsLetterSuscription(token, list, email, el){

        $.ajax({
            async: true,
            url: BASE_URI+"/newsletter",
            type: "DELETE",
            timeout: 0,
            cache: false,
            data:{
                list_id: list,
                email: email
            },
            headers: {
                "Authorization" : token,
                "Content-Type" : "application/x-www-form-urlencoded",
            }
        }).done(function (response) {
            el.parent().removeClass('active');
            el.children("i").html('add');
            el.children("span").html('Suscribir');
                
        }).fail(function(error) {
            
            console.log(error);
            
        });
    }

    function sort_newsletter(a, b){
        return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;    
    }


    $("#nbItemsNewsletter").on("click",".nb_item button",function(e){
        
        var list = $(this).data('tag');
        var email = firebase.auth().currentUser.email;
        var user = firebase.auth().currentUser;
        var el = $(e.target).parent();

        if($(el).parent().hasClass("active")){

            user.getIdToken().then(function(token) {
                removeNewsLetterSuscription(token, list,email, el)
            });

        }else{

            user.getIdToken().then(function(token) {
                subscribePush(email, list , el, token);
            });
        }
        
    });

    
    function loadNotifications(token,other=null){
        $.ajax(
            {
                async: true,
                cache: false,
                url: BASE_URI+"/notifications/categories/list",
                type: "GET",
                timeout: 0,
                processData: true,
                headers: {
                    "Authorization": token
                }
            }
        ).done(function (response) {
            OneSignal.isPushNotificationsEnabled(function(isEnabled) {
                if (isEnabled) {
                    
                    OneSignal.getUserId( function(userId) {

                        oneSignalId = userId;
                        
                        var els = 0;

                        $("#nbItemsNotifications .lds-topics").show();
                        $.each(response.data, function( key, value ) {
                            els++;
                                if(other != null){
                                    var found = $.map(other, function(val) {
                                        return val.tag_name == value.slug ? val.tag_name: null ;
                                    });

                                    if(found[0] != value.slug){
                                        $("#nbItemsNotifications").sort(sort_newsletter).append(
                                            createNewsletterHtml(value,"add", "Suscribir")
                                        );
                                    }else{
                                        $("#nbItemsNotifications").sort(sort_newsletter).append(
                                            createNewsletterHtml(value,"check", "Suscrito")
                                        );
                                    }
                                }else{
                                    $("#nbItemsNotifications").sort(sort_newsletter).append(
                                        createNewsletterHtml(value,"add", "Suscribir")
                                    );
                                }
                            if(els == response.data.length){
                                $("#nbItemsNotifications .lds-topics").hide();
                                $("#nbItemsNotifications").html(
                                    $("#nbItemsNotifications").children('div').sort(function(a, b){
                                        return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
                                    })
                                );
                            }
                        });
                    });
                }else{
                    $("#nbItemsNotifications").html("<div class='col-md-12'><p>No se encuentra suscrito a notificaciones push.</p> <a href='#' id='onesignal-button'>Suscribir a las Notificaciones</a></div>");
                }

            });
            
        }).fail(function(error) {
            console.log(error);
        });
    }

    function loadMyNotifications(token){

        var user = firebase.auth().currentUser;

        //OneSignal.isPushNotificationsEnabled(function(isEnabled) {
            //if (isEnabled) {
                
                //OneSignal.getUserId( function(userId) {
                    $.ajax({
                        async: true,
                        url: BASE_URI+"/notifications",
                        type: "GET",
                        timeout: 0,
                        cache: false,
                        headers: {
                            "Authorization" : token,
                            "Content-Type" : "application/x-www-form-urlencoded",
                        }
                    }).done(function (response) {
                        
                        loadNotifications(token, response.data);
                            
                    }).fail(function(error) {
                        
                        console.log(error);
                        
                    });
                //});
           // }/*else{
               /* $("#nbItemsNotifications").html("<div class='col-md-12'>No se encuentra suscrito a notificaciones push. <div class='onesignal-customlink-container'><a href='#' id='onesignal-button' style='display: none;'>Subscribe to Notifications</a></div></div>");
                
            }*/
        //});
    }

    $("#nbItemsNotifications").on("click","#onesignal-button", function(e){
        
        OneSignal.push(["getNotificationPermission", function(permission) {
            console.log("Site Notification Permission:", permission);
            // (Output) Site Notification Permission: default
        }]);
        OneSignal.on('notificationPermissionChange', function(permissionChange) {
            var currentPermission = permissionChange.to;
            console.log('New permission state:', currentPermission);
          });
        //OneSignal.isPushNotificationsEnabled(function(isEnabled) {
            OneSignal.push(function() {
                OneSignal.showNativePrompt();
                console.log("inside push");
            });
        //});
        console.log("click");
        e.preventDefault();
    });

    function loadUserFavorites(token, offset=0, limit){
        
        $.ajax(
            {
                async: true,
                cache: false,
                url: BASE_URI+"/favorites",
                type: "GET",
                timeout: 0,
                processData: true,
                data:{
                    limit: limit,
                    offset: offset
                },
                headers: {
                    "Authorization": token
                }
            }
        ).done(function (response) {
                
            var els = 0;
            var total = parseInt($("#list_favorites .favorites_content").children(".ag_side_post").length);

            if(response.offset < response.total ){
                $("#list_favorites .favorites_content").html("");
                $.each(response.data, function( key, value ) {

                    els++;

                    //if(els < response.data.length){
                        $("#list_favorites .favorites_content").last().append(
                        
                            createFavoriteHtml(value)

                        );
                    //}

                    if(els == response.data.length){
                        $("#list_favorites .favorites_content .lds-topics").hide();
                    }

                });
            
            }
            if(response.data.length == 0){
                $("#list_favorites .favorites_content .lds-topics").hide();
                $("#list_favorites .favorites_content").append("<p>Usted no posee favoritos hasta el momento.</p>");
                $(".fwp.ag_more").hide();
                $("#favCounter.countBigTag").html(0);
            }
        });
    }

    function loadSuggestedTopics(token, other = null){
        var settings = {
            "async": true,
            "cache": false,
            "url": BASE_URI+"/topics/suggested",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            
            var els = 0; 

            $.each(response.data, function( key, value ) {

                if(other != null){
                    var found = $.map(other, function(val) {
                        return val.topic_name == value.topic_name ? val.topic_name: null ;
                    });

                    if(found[0] != value.topic_name){
                        $("#suggested_topics").append(
                        
                            createTopicHtml(value.title, value.topic_name, value.title, "add", "")
                        );
                    }
                }else{

                    $("#suggested_topics").append(
                        
                        createTopicHtml(value.title, value.topic_name, value.title, "add", "")
                    );
                }

                els++;
                if(els ==response.data.length){
                    $("#suggested_topics .lds-topics").hide();
                    
                }
            });

            if(response.data.length == 0){
                $("#suggested_topics  .lds-topics").hide();
                $("#suggested_topics ").append("<p>Usted no posee t&oacute;picos hasta el momento.</p>");
            }
        });
    }

    function noRecords(parent = "#list_topics"){
        var empty = $(parent).children(".temaBTN1").length;
        if(empty <= 0){
            if(parent == "#suggested_topics" || parent == "#list_topics"){
                $(parent).append("<p>Sin t&oacute;picos disponibles</p>");
            }
            $("#myTopicsCount").html(0);
        }else{
            $("#myTopicsCount").html(empty);
        }
    }

    $("#nbItemsNotifications").on("click",".nb_item button", function(){
        var title = $(this).data('title');
        var tag_name = $(this).data('tag');
        var el = $(this);

        if($(this).parent().hasClass('active')){
            removeNotification(tag_name, $(this));
        }else{
            addNotification(title, tag_name,$(this));
        }
    });

    function removeNotification(tag_name, el){

        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {

            OneSignal.isPushNotificationsEnabled(function(isEnabled) {
                if (isEnabled) {
                    
                    OneSignal.getUserId( function(userId) {
                        $.ajax({
                            async: true,
                            url: BASE_URI+"/notifications",
                            type: "DELETE",
                            timeout: 0,
                            cache: false,
                            data:{
                                tag_name: tag_name,
                                player_id: userId,
                                app_id: app_id
                            },
                            headers: {
                                "Authorization" : token,
                                "Content-Type" : "application/x-www-form-urlencoded",
                            }
                        }).done(function (response) {
                            el.parent().removeClass('active');
                            el.children("i").html('add');
                            el.children("span").html('Suscribir');
                                
                        }).fail(function(error) {
                            
                            console.log(error);
                            
                        });
                    });
                }else{
                    $("#nbItemsNotifications").html("<div class='col-md-12'>No se encuentra suscrito a notificaciones push.</div>");
                    OneSignal.showNativePrompt();
                }
            });
        });

    }


    function addNotification(title,tag_name, el){
        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {

            OneSignal.isPushNotificationsEnabled(function(isEnabled) {
                if (isEnabled) {
                    
                    OneSignal.getUserId( function(userId) {
                        $.ajax({
                            async: true,
                            url: BASE_URI+"/notifications",
                            type: "POST",
                            timeout: 0,
                            cache: false,
                            data:{
                                tag_name: tag_name,
                                title: title,
                                player_id: userId,
                                app_id: app_id
                            },
                            headers: {
                                "Authorization" : token,
                                "Content-Type" : "application/x-www-form-urlencoded",
                            }
                        }).done(function (response) {
                                                        
                            el.children("i").html("check");
                            el.css({"background-color":"transparent","border":"1px solid #007550"});
                            el.children().css("color","#007550");
                            el.children("span").html('Suscrito');
                                
                        }).fail(function(error) {
                            
                            console.log(error);
                            
                        });
                    });
                }
            });

        });
    }
    

    function addTopic(name, title){
        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {

            $.ajax({
                    async: true,
                    url: BASE_URI+"/topics",
                    type: "POST",
                    timeout: 0,
                    cache: false,
                    data:{
                        topic_name: name,
                        title: title
                    },
                    headers: {
                        "Authorization" : token,
                        "Content-Type" : "application/x-www-form-urlencoded",
                    }
            }).done(function (response) {

                    
            }).fail(function(error) {

                
            });

        });
    }

    function removeTopic(topic_name){

        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {
            
            $.ajax({
                async: true,
                url: BASE_URI+"/topics",
                method: "DELETE",
                timeout: 0,
                cache: false,
                data:{
                    topic_name: topic_name
                },
                headers: {
                    Authorization: token
                }
            }).done(function (response) {
                
                
            }).fail(function(error) {

                
            });

        });
        
    }

    function getAllTopicsSearch(tk){

        $.ajax({

            type: "GET",
            cache:false,
            async: true,
            url: BASE_URI+"/topics/suggested",
            dataType: "json",
            headers: {
                Authorization: tk
            }

        }).done(function (response) {
            var els = 0;
            $.each(response.data, function( key, value ) {
                
                allTopics.push({"value":value.topic_name, "label":value.title});
                els++;
            });

            if(allTopics.length == response.data.length){
                $("#topicsearch").prop("disabled", false);
            }
            
        }).fail(function(error) {


        });

    }

    function removeFavorite(id=null){

        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {
            
            $.ajax({
                async: true,
                url: BASE_URI+"/favorites",
                method: "DELETE",
                timeout: 0,
                cache: false,
                data:{
                    news_id: id
                },
                headers: {
                    Authorization: token
                }
            }).done(function (response) {
                if($(".favorites_content").children().length == 0){
                    $("#list_favorites .favorites_content").html('<div class="lds-topics"> <div></div> <div></div> <div></div> </div>');
                    loadUserFavorites(token,offset,10);
                }
                
            }).fail(function(error) {

                
            });

        });
        
    }


    $( "#topicsearch" ).autocomplete({
        
        source: allTopics,
        focus: function( event, ui ) {
            $( "#topicsearch" ).val( ui.item.label );
            return false;
        },
        minLength: 1,
        select: function( event, ui ) {

            
            var exist = $("#list_topics").find('[data-name="'+ui.item.value+'"]').length;
            
            if(exist == 0){

                $("#list_topics").append(
                    createTopicHtml(ui.item.value, ui.item.label, ui.item.value, "check", "active")
                );

                addTopic(ui.item.value,ui.item.label);
            }

            $( this ).val( ui.item.label );
            return false;

        }

    });

    $(".loadmoreBTN").on("click", function(){
        
        var user = firebase.auth().currentUser;

        user.getIdToken().then(function(token) {
            loadUserFavorites(token,offset,10);
        });
        
    });


    $("#list_topics").on("click",".temaBTN1 .tbtn", function(e){

        var name = $(this).parent().data('name');
        var title = $(this).parent().data('title');
        var clone_el = $(this).parent().clone();
        clone_el.children(".tbtn").toggleClass( "active" ).children().html("add");

        $("#suggested_topics p").remove();
        $("#suggested_topics").append(clone_el);
        $(this).parent().remove();
        
        removeTopic(name,title);
        
        noRecords("#list_topics");

    });

    $(document).delegate("#list_topics .temaBTN1 .tbtn","mouseover", function(){
        $(this).children("i").html("clear");
    });

    $(document).delegate("#list_topics .temaBTN1 .tbtn","mouseout", function(){
        $(this).children("i").html("check");
    });

    $("#suggested_topics").on("click",".temaBTN1 .tbtn", function(e){

        var name = $(this).parent().data('name');
        var title = $(this).parent().data('title');
        var clone_el = $(this).parent().clone();
        clone_el.children(".tbtn").toggleClass( "active" ).children().html("check");

        $("#list_topics p").remove();
        $("#list_topics").append(clone_el);
        $(this).parent().remove();

        addTopic(name,title);
        
        noRecords("#list_topics");
    });

    $("#list_favorites").on("click", ".pdm_button",function(){
        var id = $(this).closest(".ag_side_post").data("id");
        $(this).closest(".ag_side_post").remove();
        
            removeFavorite(id);
       
        $("#favCounter").html(parseInt($("#favCounter").html()-1));
    });

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            linkedAccount(user);
            user.getIdToken().then(function(token) {
                tk = token;
                console.log(token);
                if(token){ 
                    loadUserData(token);
                    loadCountries(token);
                    loadUserTopics(token);
                    totalFavorites(token);
                    loadUserFavorites(token, 0, 3);
                    getAllTopicsSearch(token);
                    //loadNewsLetters(token);
                    //loadNotifications(token);
                    loadMyNotifications(token);
                    //fillSideBar(user);
                    loadMyNewsLetters(token);
                    
                }
                
            });
        }else{
            window.location.replace("login.html");
        }
    });

});