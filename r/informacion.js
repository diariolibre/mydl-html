const googleProvider = new firebase.auth.GoogleAuthProvider();
const facebookProvider = new firebase.auth.FacebookAuthProvider();

function linkGoogle() {                
        
    firebase.auth().currentUser.linkWithPopup(googleProvider).then(function(result) {
        // Accounts successfully linked.
        var credential = result.credential;
        var user = result.user;
        linkedAccount(firebase.auth().currentUser);
        window.location.reload();
    }).catch(function(error) {
        
        console.log(error);
        if(error.code == "auth/credential-already-in-use"){
            $(".alert-box").show(100).children("span").html("Esta cuenta ya se encuetra asociada a otro usuario.");
        }
        

    });
}

function linkFacebook() {
    
    firebase.auth().currentUser.linkWithPopup(facebookProvider).then(function(result) {
        // Accounts successfully linked.
        var credential = result.credential;
        var user = result.user;
        linkedAccount(firebase.auth().currentUser);
        window.location.reload();
    }).catch(function(error) {
        console.log(error);
        if(error.code == "auth/credential-already-in-use"){
            $(".alert-box").show(100).children("span").html("Esta cuenta ya se encuetra asociada a otro usuario.");
        }

    });
}


function unLinkAccount(providerId) {

    firebase.auth().currentUser.unlink(providerId).then(function() {
        linkedAccount(firebase.auth().currentUser);

        console.log("unlinked");
        if(providerId == "google.com"){
            $(".sbr_btn.gl span").html("Google");
            $(".sbr_btn.gl").attr("onclick","linkGoogle()");
        }
        if(providerId == "facebook.com"){
            $(".sbr_btn.fb span").html("Facebook");
            $(".sbr_btn.fb").attr("onclick","linkFacebook()");
        }

        window.location.reload();
        
    }).catch(function(error) {
    
        console.log(error);
        console.log("No se hizo");
        alert(error);
    });
    
}

function linkedAccount(user){
    var providerData = user.providerData;
    
    if(providerData.length > 0){

        const google = providerData.find( ({ providerId }) => providerId === 'google.com' );
        const facebook = providerData.find( ({ providerId }) => providerId === 'facebook.com' );

        if(typeof google !== "undefined"){
            $(".sbr_btn.gl span").html("Desvincular");
            $(".sbr_btn.gl").attr("onclick","unLinkAccount('google.com')");
        }
        if(typeof facebook !== "undefined"){
            $(".sbr_btn.fb span").html("Desvincular");
            $(".sbr_btn.fb").attr("onclick","unLinkAccount('facebook.com')");
        }

    }else{
        $(".sbr_btn.gl span").html("Google");
        $(".sbr_btn.gl").attr("onclick","linkGoogle()");
        $(".sbr_btn.fb span").html("Facebook");
        $(".sbr_btn.fb").attr("onclick","linkFacebook()");
    }

}

$(function(){

    var tmp_avatar = getAvatar();


    function loadUserData(token){

        var settings = {
            "async": true,
            "url": BASE_URI+"/accounts?",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            console.log("response",response);

            var authSurname = null;

            if(response.data != null){
                
                var gender = response.data.gender;
                var birth_day = response.data.birth_day;
                var country  = response.data.country;
                authSurname = response.data.last_name;

                $("#auth-surname").val(authSurname);
                console.log("country", response.data.country);
                console.log("firebase",firebase.auth().currentUser);

                $("select[name='pais']").children("option[value='"+country+"']").prop('selected', true);

                //$("select[name='pais']").val(country);
                $("input[name='date']").val(birth_day);
                $("select[name='genero']").val(gender);
                
            }

            var authEmail = firebase.auth().currentUser.email;
            var authName = firebase.auth().currentUser.displayName;

            $('.hui_info b').html(authName + " " + authSurname);
            $("#auth-email").val(authEmail);
            $("#auth-name").val(authName);

            if(firebase.auth().currentUser.providerData.length > 1){
                
                $.each(firebase.auth().currentUser.providerData, function(index, value){

                        switch(value.providerId){
                            case "facebook.com":
                                $('.bg_cover').css('background-image','url('+value.photoURL+')');
                                break;
                            case "google.com":
                                $('.bg_cover').css('background-image','url('+value.photoURL+')');
                                break;
                        }
                    
                });

            }else{

                firebase.auth().currentUser.photoURL !== null ? 
                    $('.bg_cover').css('background-image','url('+firebase.auth().currentUser.photoURL+')') : 
                    $('.bg_cover').css('background-image','url('+tmp_avatar+')');

            }
        });

    }


    function ValidateForm() {
        
        var authName = $("#auth-name").val();
        var authSurname = $("#auth-surname").val();
        
        var authEmailService = firebase.auth().currentUser.email;
        var authPhoto = firebase.auth().currentUser.photoURL;

        $.validator.addMethod("dateFormat", function(value,element) {
            return value.match(/^(0[1-9]|1[012])[- //.](0[1-9]|[12][0-9]|3[01])[- //.](19|20)\d\d$/);
        }, "Please enter a date in the format mm/dd/yyyy");

        $.validator.addMethod("noSpace", function (value, element) {
            return this.optional(element) || /.*\S+.*/.test(value);
        }, "No se permite introducir espacios en blanco");

        var validator = $("#changes").validate({
            rules: {
                auth_name: {
                    required: true,
                    noSpace: true
                },
                auth_surname: {
                    required: true,
                    noSpace: true
                },
            },
            messages:{
                date:"Por favor introduzca una fecha válida"
            }

        });
        
        if ((authName != "") && (authSurname != "")) {
            
            if (validator.form()) {
                $(".sbr_input").removeClass("error-field");
                $(".alert-box").hide(100);
                return true;  
            } else {
                $(".alert-box").show(100);
                return false;
            }              
            
        }

    }

    function showError(error) {
        $(".alert-box span").text(error);
        $(".sbr_input").addClass("error");
        $(".alert-box").show(100);
    }


    function register() {

        firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
            sendRegisterWithToken(idToken);

        }).catch(function(error) {
            
            console.log(error);
        });
    }


    function sendRegisterWithToken(token) {

        var authName = $("#auth-name").val();
        var authSurname = $("#auth-surname").val();
        var country  = $("select[name='pais']").children("option:selected").val();
        var birthDay = $("input[name='date']").val();
        var gener = $("select[name='genero']").children("option:selected").val();
        var authPhoto = null;

        if(firebase.auth().currentUser.providerData.length > 1){

            $.each(firebase.auth().currentUser.providerData, function(index, value){
                if(value.providerId !== "password"){

                    authPhoto = value.photoURL !== null ? value.photoURL : tmp_avatar;

                }
                
            })

        }else{
            authPhoto = firebase.auth().currentUser.photoURL == null ? tmp_avatar : firebase.auth().currentUser.photoURL;
        }

        var authEmailService = firebase.auth().currentUser.email == null ? $("#auth-email").val() : firebase.auth().currentUser.email;

        var validate = ValidateForm();
        if(validate == true) {
            var settings = {
                    "url": BASE_URI+"/accounts",
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": token
                },
                    "data": {
                    "name": authName,
                    "last_name": authSurname,
                    "email": authEmailService,
                    //"avatar": authPhoto,
                    "country": country,
                    "birth_day": birthDay,
                    "gender":gener
                }
            };

            $.ajax(settings).done(function (response) {


                firebase.auth().currentUser.updateProfile({
                    photoURL: authPhoto,
                    displayName: authName
                }).then(function() {
                    // Update successful.
                }).catch(function(error) {
                    // An error happened.
                });

                console.log(response);
                //loadUserData(token);
                location.reload(true);


            }) .fail(function(error) {
                console.log(error);
                var errorMsg = error.responseJSON.message;
                showError(errorMsg);
                $(".alert-box").show(100);
            });
        } else {
            return false;
        }
    }

    $(".ucbrc_submit").on("click", function(){
                        
        register();
            
    });

    $("#changes").submit(function(e) {
        e.preventDefault();
    });


    
                

    

    function logout(){
        firebase.auth().signOut().then(function() {
            window.location.replace("login.html");
        }, function(error) {
            console.log(error);
        });
    }

    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log(user);

            linkedAccount(user);

            user.getIdToken().then(function(data) {
                //console.log(data)
                loadUserData(data);
            });
                
        } else {
            window.location.replace("login.html");
        }
    });


    function unLink() {
        var provider = new firebase.auth.EmailAuthProvider();
        firebase.auth().currentUser.unlink(provider.providerId).then(function() {
            // Auth provider unlinked from account
            // ...
            //console.log(user);
            //console.log("Se hizo");
        }).catch(function(error) {
            // An error happened
            // ...
            console.log(error);
            //console.log("No se hizo");
        });
    }

});