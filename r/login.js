var loginNow = false;
firebase.auth().languageCode = 'es';

function callGoogleSignIn(){
    var provider = new firebase.auth.GoogleAuthProvider();
    //provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
    firebase.auth().signInWithPopup(provider).then(function(result) {
          //var token = result.credential.accessToken;
          loginNow = true;
   }).catch(function(error) {
         var errorCode = error.code;
         var errorMessage = error.message;
         var email = error.email;
         var credential = error.credential;
         $("#loginForm .sbr_btn.gl.social").html('<img src="r/img/icons/w.gl.svg" alt=""> Google').removeAttr("disabled");
   });
}

function facebookSignIn(event) {
    //console.log(event.target.innerHTML());
   var provider = new firebase.auth.FacebookAuthProvider();
   //provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
   firebase.auth().signInWithPopup(provider)

   .then(function(result) {
    
      loginNow = true;
      
   }).catch(function(error) {
      console.log(error.code);
      console.log(error.message);
      $("#loginForm .sbr_btn.fb.social").html('<img src="r/img/icons/w.fb.svg" alt=""> Facebook').removeAttr("disabled");
   });
}

var bcProfile = '';
var bCClient = '';

  
  // Is BlueConic loaded?
  if (typeof window.blueConicClient !== 'undefined' &&
      typeof window.blueConicClient.event !== 'undefined' &&
      typeof window.blueConicClient.event.subscribe !== 'undefined') {
        profile = blueConicClient.profile.getProfile();
        bcProfile = profile;
        bCClient = blueConicClient;
        var properties = ['DL_Name', 'bc_dev_login_address','DL_Nationality','OneSignal_Player_ID'];
      profile.loadValues(properties, this, function() {
        var DL_Name = profile.getValue('DL_Name');
        var bc_dev_login_address = profile.getValue('bc_dev_login_address');
        var DL_Nationality = profile.getValue('DL_Nationality');
        console.log('DL_Name:', DL_Name);
        console.log('bc_dev_login_address:', bc_dev_login_address);
        console.log('profileid:', profile.getId());
        console.log('OneSignal_Player_ID:', profile.getValue('OneSignal_Player_ID'));
      });
      
  } else {
    // Not yet loaded; wait for the "onBlueConicLoaded" event
    window.addEventListener('onBlueConicLoaded', function () {
      // BlueConic is loaded, now we can do API things
      profile = blueConicClient.profile.getProfile();
      bcProfile = profile;
      bCClient = blueConicClient;
      var properties = ['DL_Name', 'bc_dev_login_address','DL_Nationality','OneSignal_Player_ID'];
      profile.loadValues(properties, this, function() {
        var DL_Name = profile.getValue('DL_Name');
        var bc_dev_login_address = profile.getValue('bc_dev_login_address');
        var DL_Nationality = profile.getValue('DL_Nationality');
        console.log('DL_Name:', DL_Name);
        console.log('bc_dev_login_address:', bc_dev_login_address);
        console.log('profileid:', profile.getId());
        console.log('OneSignal_Player_ID:', profile.getValue('OneSignal_Player_ID'));
      });
    }, false);
}

$(function(){

    var token;

    function updateBcProfile(token, is_complete){

        var is_email_bc='';

        if($.trim(bcProfile.getValues('bc_dev_login_address')) == $.trim(firebase.auth().currentUser.email)){
            is_email_bc = 'reconfirmado';
        }else{
            bcProfile.setValue("bc_dev_login_address", $.trim(firebase.auth().currentUser.email));                    
            bCClient.profile.updateProfile();
            is_email_bc = 'actualizado';
        }

        var settings = {
            "url": BASE_URI+"/accounts",
            "method": "PUT",
            "timeout": 0,
            "cache": true,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": token
            },
            "data": {
                "bc_profileid": bcProfile.getId(),
                "onesignal_playerid": bcProfile.getValue('OneSignal_Player_ID'),
                "is_email_bc": is_email_bc,
                "email":$.trim(firebase.auth().currentUser.email),
                //"is_complete": is_complete
            }
        }
        
        $.ajax(settings).done(function (response) {
            console.log(response);
        }).fail(function(jqXHR, textStatus, errorThrown ){
            console.log(textStatus);
            console.log(jqXHR.responseJSON);
            console.log(errorThrown);
        });

        //return false;
    }

    $("#registerForm, #loginForm").submit(function(e) {
        e.preventDefault();
    });
    
    function showAlert(errorMsg, field) {
        $(".sbrSignupCont .sbr_input").removeClass("error");
        $(".sbrSignupCont .alert-box span").text(errorMsg);
        if (field == "email") {
            $("#auth-email").addClass("error");
        } else if (field == "password") {
            $("#auth-pass").addClass("error");
            $("#auth-pass2").addClass("error");
        } else if (field == "all") {
            $(".sbrSignupCont .sbr_input").addClass("error");
        } else {
            $(".sbrSignupCont .sbr_input").addClass("error");
        }
        $(".sbrSignupCont .alert-box").show(100);
    }
        
    function showAlertLogin(errorMsg, field) {
        $(".sbrLoginCont .sbr_input").removeClass("error");
        $(".alert-box-login span").text(errorMsg);
        if (field == "email") {
            $(".sbrLoginCont #auth-email-login").addClass("error");
        } else if (field == "password") {
            $(".sbrLoginCont #auth-pass-login").addClass("error");
        } else if (field == "all") {
            $(".sbrLoginCont .sbr_input").addClass("error");
        } else {
            $(".sbrLoginCont .sbr_input").addClass("error");
        }
        $(".alert-box-login").show(100);
    }
        
        
    function firebaseAuth(authEmail, authPass, authPass2) {
        if (authPass != "" && authPass == authPass2) {
            firebase.auth().createUserWithEmailAndPassword(authEmail, authPass)
            .then(function(result) {
                loginNow = true;
                //updateBcProfile(token);
            }).catch(function(error) {

                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(error);

                var values = showErrorMsg(errorCode);
                var first = values[0];
                var second = values[1];
                
                showAlert(first, second);
                
                return false;
            });
        } else {
                
            var first = "Las contraseñas no coinciden";
            var second = "password";
            showAlert(first, second);
                
            return false;

        }    
    }

    $("#auth-email-login").on('keyup change',function(){
        var value = $(this).val();
        var domain = value.substr(value.indexOf("@") + 1);
        var tld = domain.split(".").pop();

        if(tld.length > 1){
            $(".alert-box-login").hide();
        }
    });

    $("#auth-pass-login").on('keyup change',function(){

        if($(this).val().length >= 6){
            $(".alert-box-login").hide();
        }
    });
        
    function firebaseLogIn(authEmail, authPass) {
        if (authEmail != "" || authPass != "") {
            firebase.auth().signInWithEmailAndPassword(authEmail, authPass)
            .then(function(result) {
                //console.log("Se logueo");
                $(".sbrLoginCont .sbr_input").removeClass("error");
                $(".sbrLoginCont .alert-box").hide();
                loginNow = true;
                
                //updateBcProfile(token);

            }).catch(function(error) {

                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(error);
                var values = showErrorMsg(errorCode);
                var first = values[0];
                var second = values[1];
                
                showAlertLogin(first, second);
                console.log(first);
                console.log(second);
                $("#loginForm .sbr_btn.email").html("Acceder").removeAttr("disabled");
                
                return false;
            });
        } else {
                
                var first = "Debe ingresar correo y contraseña";
                var second = "all";
                showAlertLogin(first, second);
                console.log(first);
                console.log(second);
                return false;

        }     
    }

    $.validator.addMethod("noSpace", function (value, element) {
        return this.optional(element) || /.*\S+.*/.test(value);
    }, "No se permite introducir espacios en blanco");

    $.validator.methods.email = function( value, element ) {
        return this.optional( element ) || /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}/.test( value );
    }
    
    var validator = $("#loginForm").validate({
        rules: {
            auth_email_login: {
                required: true,
                email: true,
                noSpace: true
            },
            auth_pass_login:{
                noSpace: true,
                required: true,
                minlength: 6
            },
        },
        messages:{
            auth_email_login:{
                auth_email_login: "Introduzca un correo electrónico válido",
                required: "El campo Correo electrónico es requerido"
            },
            auth_pass_login:{
                required: "El campo Contraseña es requerido"
            }
        },
        errorPlacement: function(error, element) {
            $(".alert-box-login span").html("");
            error.appendTo($(".alert-box-login").show().children("span"));           
        } 

    });
    
        
    function emailLogIn() {
        logoutAuth();
        var authEmail = $("#auth-email-login").val();
        var authPass = $("#auth-pass-login").val();
        
        firebaseLogIn(authEmail, authPass);

        return ;
    }

    $( "#loginForm" ).submit(function( event ) {
        if($( "#loginForm" ).valid()) {

            emailLogIn();
            $(this).children(".sbr_btn").html("<img src='r/img/loading.gif'>").attr("disabled", 'disabled');
        }
        event.preventDefault();
    });
    
    $('.sbr_input').keypress(function (e) {
      if (e.which == 13 && $("#auth-email").val() != '') {
        emailSignIn();
        e.preventDefault();
      }
      
    });

    $("#auth-email-login").keydown(function (e) {
        if (e.keyCode == 32) { 
          $(this).val($(this).val() + "");
          return false; 
        }
   });
    
    $(".logout-btn").click(function() {
        logoutAuth();
        location.reload(true);
    });

    $(".sbrLoginCont").on("click",".overlayForm .logout", function(){
        logoutAuth();
        location.reload(true);
    });

    function loadUserData(token){
        
        var settings = {
            "url": BASE_URI+"/accounts?",
            "method": "GET",
            "timeout": 0,
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": token
            },
        };
    
        $.ajax(settings).done(function (response) {
            
            console.table("response.data",response.data);
            
            if(response.hasOwnProperty('data')){

                updateBcProfile(token, response.data.is_complete);

                if((response.data.hasOwnProperty('is_complete') && response.data.is_complete == false) || firebase.auth().currentUser.email == null){
                    window.location.replace(BASE_SITE+"/signup-confirm.html");
                }else{
                    window.location.replace(BASE_SITE+"/perfil-informacion.html");
                }
            }else{
                window.location.replace(BASE_SITE+"/signup-confirm.html");
            }
        });
    }

    function overlayForm(){
        $(".signupBox .sb_right").addClass("overlayFormFlex");
        $(".sbr_title.fwp").html("Usted ya se encuentra logueado");
        var html = '<div class="overlayForm">';
                html += '<a href="#" class="sbr_btn email logout">Entrar con otras credenciales</a>';
                html += '<a href="mi-dl.html" class="sbr_btn email">Ir a MiDL</a>';
            html += '</div>';

        return html;
    }


    firebase.auth().onAuthStateChanged(function(user) {
        $(".fwp.sbrLoginCont").show();
        $(".sbr_title").show();
        var tk;
        if (user) {
            $(".sbr_input").removeClass("error-field");
            $(".alert-box").hide(100);
            var authName = firebase.auth().currentUser.displayName;
    
            var emailVerified =  firebase.auth().currentUser.emailVerified;
        
            if(!emailVerified){
                user.sendEmailVerification();
            }

           
                user.getIdToken().then(function(data) {

                    if(loginNow){
                        loadUserData(data);
                    }else{
                        $(".fwp.sbrLoginCont").html(overlayForm());
                        //loadUserData(data);
                    }
                    //updateBcProfile(data);
                    token = data;
                    
                });
            
        } 
    });

 });