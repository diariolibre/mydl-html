function showErrorMsg(error) {
                var errorMsg = "";
                var errorField = "";
                var fieldAll = "all";
                var fieldEmail = "email";
                var fieldPass = "password";
            switch (error) {
                case "auth/provider-already-linked":
                    errorMsg = "Estas credenciales ya están enlazadas.";
                    return [errorMsg, fieldAll];
                break;
                case "auth/invalid-credential":
                    errorMsg = "Credenciales inválidas.";
                    return [errorMsg, fieldAll];
                break;
                case "auth/credential-already-in-use":
                    errorMsg = "Estas credenciales ya están registradas.";
                    return [errorMsg, fieldAll];
                break;
                case "auth/email-already-in-use":
                    errorMsg = "Este correo ya está registrado.";
                    return [errorMsg, fieldEmail];
                break;
                case "auth/operation-not-allowed":
                    errorMsg = "Esta operación no está permitida.";
                    return [errorMsg, fieldAll];
                break;
                case "auth/invalid-email":
                    errorMsg = "El correo está incorrecto.";
                    return [errorMsg, fieldEmail];
                break;
                case "auth/wrong-password":
                    errorMsg = "Contraseña inválida.";
                    return [errorMsg, fieldPass];
                break;
                case "auth/invalid-verification-code":
                    errorMsg = "Código de verificación inválido.";
                    return [errorMsg, fieldAll];
                break;
                case "auth/user-not-found":
                    errorMsg = "No hay registro con estas credenciales.";
                    return [errorMsg, fieldAll];
                break;
                case "auth/weak-password":
                    errorMsg = "Contraseña muy débil.";
                    return [errorMsg, fieldPass];
                break;
                case "auth/too-many-requests":
                    errorMsg = "Demasiados intentos fallidos. Inténtelo más tarde.";
                    return [errorMsg, fieldAll];
                break;
                case "auth/invalid-action-code":
                    errorMsg = "El código de acción es inválido. Esto puede suceder si el código está mal formado, expirado o ya ha sido usado";
                    return [errorMsg, fieldAll];
                break;
                case "auth/expired-action-code":
                    errorMsg = "El código de acción ha expirado.";
                    return [errorMsg, fieldAll];
                break;
                case "auth/network-request-failed":
                    errorMsg = "Se ha producido un error de red (como tiempo de espera, conexión interrumpida o host inaccesible).";
                    return [errorMsg, fieldAll];
                break;
                default:
                    errorMsg = "Lo sentimos, ha ocurrido un error."+" - "+error;
                    return [errorMsg, fieldAll];
                    }
              }