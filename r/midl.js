var tk = '';
function createTopicHtml(data, icon='check'){

    var html = '<div class="topicwrapper" data-topic="'+data.topic_name+'">';
        html +='<div class="container">';
        html +='<div class="row">';
        html +='<div class="temaBTN1">';
            html +='<a href="https://www.diariolibre.com/cronologia/ver/meta/'+data.topic_name+'"><span>'+data.title+'</span></a>';
            html +='<span class="tbtn_separador"></span>';
            html +='<button class="tbtn active"><i class="material-icons">'+icon+'</i></button>';
        html +='</div>';
        html +='</div>';
    html +='</div>';

    html+='<iframe src="https://www.diariolibre.com/cronologia-midl-temas/ver/meta/'+data.topic_name+'" style="width:100%"></iframe>';
    html +='</div>';
    return html;
}

function createSuggestedTopicHtml(data, icon='add'){

    var status ='';
    if(icon == 'check'){
        status = 'active';
    }

    var html ='<div class="temaBTN1" data-topic="'+data.topic_name+'">';
			html+='<a href="https://www.diariolibre.com/cronologia/ver/meta/'+data.topic_name+'"><span>'+data.title+'</span></a>';
				html+='<span class="tbtn_separador"></span>'
				html+='<button class="tbtn '+status+'"><i class="material-icons">'+icon+'</i></button>';
		html+='</div>';

    return html;
}

function createFavoriteHtml(data, index){
    
    var els = 0;
   
    $.each(data, function( index, value ) {
        
        html = '<div class="fwp">';
            html +='<a href="'+value.url+'">';
                html +='<div class="bg_cover" style="background-image: url('+value.imagen+');"></div>';
            html +='</a>';
            if(index==0){
                html +='<div class="agmpi_info fwp">';
            }else{
                html +='<div class="agsp_info">';
            }
                html +='<a href="'+value.url+'">';
                    html +='<h2 class="agmpi_title">'+value.title+'</h2>';
                html +='</a>';
                html +='<div class="fwp">';
                    html +='<a href="https://www.diariolibre.com/cronologia/ver/meta/'+value.topic_name+'" class="tagLink">'+value.topic_title+'</a>';
                html +='</div>';
            html +='</div>';
        html +='</div>';


        if(index==0){
            $("#articulosGuardados .lds-topics").hide();
            $("#articulosGuardados #leftSide").append(html).show().children('div:first').addClass('ag_main_post_item').children("div.ag_main_post_item");
            $("#articulosGuardados #leftSide .ag_main_post_item a .bg_cover").addClass("agmpi_img");
        }else{
            $("#articulosGuardados .ag_sidePostCont").append(html).show().children('div.fwp').addClass('ag_side_post');
            $("#articulosGuardados .ag_sidePostCont a .bg_cover").addClass("agsp_img");
        }

        els++;

        if(els == data.length){
            $("#articulosGuardados .lds-topics").hide();
        }
    });

    //$(content).appendTo("#articulosGuardados");
    
        
        
        //return html;
}
function loadUserTopics(token, limit=3){

    tk = token;
    var actualTopics = [];

    var settings = {
        "async": true,
        "cache": false,
        "url": BASE_URI+"/topics",
        "method": "GET",
        "timeout": 0,
        "data":{
            "limit":limit
        },
        "headers": {
        "Authorization": token
        },
    };

    $.ajax(settings).done(function (response) {
        
        console.table(response.data);
        
        $.each($("#list_topics .topicslist").children(".topicwrapper"), function(key, value){
        
            actualTopics.push({topic_name:$(value).data('topic')});

        });

        var els = 0; 
       
        $.each(response.data, function( key, value ) {

           
            if(actualTopics.length > 0){
                var found = $.map(actualTopics, function(val) {
                    return val.topic_name == value.topic_name ? val.topic_name: null ;
                });

                if(found[0] != value.topic_name){
                    $("#list_topics .topicslist").append(
                    
                        createTopicHtml(value)
                    );
                }
    

            }else{
                $("#list_topics .topicslist").append(

                    createTopicHtml(value)
    
                );

            }

            els++;
            if(els == response.data.length){
                $("#list_topics .topicslist .lds-topics").hide();
                $("#poolBtns .poolBtns").children(".temaBTN1").remove();
                loadOtherUserTopics(token, response.data);
                $("#poolBtns .poolBtns").children("[data-topic='"+value.topic_name+"']").remove();

            }

        });

        if(response.data.length == 0){
            $("#list_topics .topicslist .lds-topics").hide();
            $("#list_topics").append("<p>Usted no posee t&oacute;picos hasta el momento.</p>");
            loadOtherUserTopics(token, null);
        }else{
            $('#list_topics .ag_more .lds-topics').hide();
            $(".ag_more").show();
            $(".ag_more .loadmoreBTN").show();
        }
    });
}

$("#list_topics").on("click",".ag_more .loadmoreBTN", function(){

    $(this).hide();
    $('#list_topics .ag_more .lds-topics').show();
    var limit = parseInt($('#list_topics .topicslist').children('.topicwrapper').length);

    loadUserTopics(tk,limit+1 );
    
});


function loadOtherUserTopics(token, other = null){

    var actualTopics = [];
    
    var settings = {
        "async": true,
        "cache": false,
        "url": BASE_URI+"/topics",
        "method": "GET",
        "timeout": 0,
        "headers": {
        "Authorization": token
        },
    };

    $.ajax(settings).done(function (response) {

        console.table(response.data);

        $.each($("#poolBtns .topicslist").children(".temaBTN1"), function(key, value){
        
            actualTopics.push({topic_name:$(value).data('topic')});

        });


        var els = 0; 
        $.each(response.data, function( key, value ) {
            
            if(other != null){
                var found = $.map(other, function(val) {
                    return val.topic_name == value.topic_name ? val.topic_name: null ;
                });

                if(found[0] != value.topic_name){
                    $("#poolBtns .topicslist").append(
                    
                        createSuggestedTopicHtml(value,"check")
                    );
                }
            }

            els++;
            if(els ==response.data.length){
                $("#poolBtns .topicslist .lds-topics").hide();
                
            }

        });

        if(response.data.length == 0 || $("#poolBtns .topicslist").children(".temaBTN1").length == 0){
            $("#poolBtns .topicslist .lds-topics").hide();
            $("#poolBtns .topicslist").append("<p>Usted no posee t&oacute;picos hasta el momento.</p>");
        }
    }).fail(function(error) {
                            
        console.log(error);
        
    });
}



function loadSuggestedTopics(token, other = null){
    var settings = {
        "async": true,
        "cache": false,
        "url": BASE_URI+"/topics/suggested",
        "method": "GET",
        "timeout": 0,
        "headers": {
        "Authorization": token
        },
    };

    $.ajax(settings).done(function (response) {
        
        var els = 0; 

        $.each(response.data, function( key, value ) {

            if(other != null){
                var found = $.map(other, function(val) {
                    return val.topic_name == value.topic_name ? val.topic_name: null ;
                });

                if(found[0] != value.topic_name){
                    $("#suggested_topics").append(
                    
                        createSuggestedTopicHtml(value)
                    );
                }
            }else{

                $("#suggested_topics").append(
                    
                    createSuggestedTopicHtml(value)
                );
            }

            els++;
            if(els ==response.data.length){
                $("#suggested_topics .lds-topics").hide();
                
            }
        });

        if(response.data.length == 0){
            $("#suggested_topics .lds-topics").hide();
            $("#suggested_topics").append("<p>Usted no posee t&oacute;picos hasta el momento.</p>");
        }
    }).fail(function(error) {
                            
        console.log(error);
        
    });
}


function loadUserFavorites(token, offset=0, limit=5){
        
    $.ajax(
        {
            async: true,
            cache: false,
            url: BASE_URI+"/favorites",
            type: "GET",
            timeout: 0,
            processData: true,
            data:{
                limit: limit,
                offset: offset
            },
            headers: {
                "Authorization": token
            }
        }
    ).done(function (response) {
            
        createFavoriteHtml(response.data);

        if(response.data.length == 0){
            $("#articulosGuardados .lds-topics").hide();
            $("#articulosGuardados").append("<p>Usted no posee favoritos hasta el momento.</p>");
        }
    }).fail(function(error) {
                            
        console.log(error);
        
    });
}


firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        
        user.getIdToken().then(function(token) {
            tk = token;
            if(token){ 
                loadUserTopics(token, 3);
                loadUserFavorites(token,0,5);
                tk=token;
            }
            
        });
    }else{
        window.location.replace("login.html");
    }
});