
var userTopics = [];
var tk = "";

$(".meta_wrap .temaBTN1 button").on("click", function(){
    var title = $(this).siblings("a").data("title");
    var name = $(this).siblings("a").data("name");
    var el = $(this);

    if($(this).hasClass("following")){
        removeTopic(name, el);
    }else{
        addTopic(name,title, el);
    }

});

function getArticleDetails(token, news_id,topic_name){

    var settings = {
        "async": true,
        "cache": true,
        "url": BASE_URI+"/details/status",
        "method": "POST",
        "data":{
            topic_name: topic_name,
            news_id: news_id
        },
        "timeout": 0,
        "headers": {
            "Authorization": token
        },
    };
    $.ajax(settings).done(function (response) {

        var primaryTopic = $(".meta_wrap .temaBTN1");
        var topicName = primaryTopic.children("a").data('name');
        var topicIcon = primaryTopic.find("i");

        if(response.is_topic){
            primaryTopic.find("button").addClass("following").css({"background-color" : "transparent", "border":"solid 1px #007550"});
            topicIcon.html("check").css("color","#007550").show();
        }else{
            topicIcon.html("add").show();
        }

        if(response.is_favorite){
            $(".meta_wrap .sfav button").css({"background-color" : "transparent", "border":"solid 1px #007550"}).addClass("following").children("i").html("star").css("color","#007550").show();
        }else{
            $(".meta_wrap .sfav button").children("i").html("star").css("color","#FFF").show();   
        }
        
        console.log("details",response);

    }).fail(function(error) {

        console.log(error);
    });
    
}


function addTopic(name, title, el){
    console.log(el);
    var user = firebase.auth().currentUser;

    user.getIdToken().then(function(token) {

        $.ajax({
                async: true,
                url: BASE_URI+"/topics",
                type: "POST",
                timeout: 0,
                cache: false,
                data:{
                    topic_name: name,
                    title: title
                },
                headers: {
                    "Authorization" : token,
                    "Content-Type" : "application/x-www-form-urlencoded",
                }
        }).done(function (response) {

            el.addClass("following");
            el.children("i").html("check").css("color","#007550");
            el.css({"background-color" : "transparent", "border":"solid 1px #007550"});
                
        }).fail(function(error) {

            console.log(error);
        });

    });
}

function removeTopic(topic_name, el){

    var user = firebase.auth().currentUser;

    user.getIdToken().then(function(token) {
        
        $.ajax({
            async: true,
            url: BASE_URI+"/topics",
            method: "DELETE",
            timeout: 0,
            cache: false,
            data:{
                topic_name: topic_name
            },
            headers: {
                Authorization: token
            }
        }).done(function (response) {
            
            el.removeClass("following");
            el.children("i").html("add").css("color","#FFF");
            el.css("background-color", "#007550");
            
        }).fail(function(error) {

            
        });

    });
    
}


function saveArticle(){

    var id = "OL18578017";//$(".article_title").data("articleId");
    var title = $(".article_title").text();
    var url = window.location.pathname;
    var image = $("#main-content").find("figure").children("img").attr("src");
    var topic_name = $(".meta_wrap .temaBTN1 a").data("name");
    var topic_title = $(".meta_wrap .temaBTN1 a").data("title");

    console.log("id", id);
    console.log("title", title);
    console.log("url", url);
    console.log("image", image);
    console.log("topic_name", topic_name);
    console.log("topic_title", topic_title);

    $.ajax({

        type: "POST",
        cache:false,
        async: true,
        url: BASE_URI+"/favorites",
        
        data:{
            "news_id" : id,
            "url" : url,
            "imagen": image,
            "title": title,
            "topic_name": topic_name,
            "topic_title": topic_title
        },
        headers: {
            Authorization: tk
        }

    }).done(function (response) {
        
        console.log(response);
        $(".meta_wrap .sfav button").css({"background-color" : "transparent", "border":"solid 1px #007550"}).addClass("following").children("i").html("star").css("color","#007550").show();
        
    }).fail(function(error) {

        console.log(error.responseJSON);	

    });
}


$(".meta_wrap .sfav button").on("click", function(){
    
    saveArticle();

});


function removeFavorite(id=null){

    var user = firebase.auth().currentUser;

    user.getIdToken().then(function(token) {
        
        $.ajax({
            async: true,
            url: BASE_URI+"/favorites",
            method: "DELETE",
            timeout: 0,
            cache: false,
            data:{
                news_id: id
            },
            headers: {
                Authorization: token
            }
        }).done(function (response) {
            
            loadUserFavorites(token,offset,10);
            
        }).fail(function(error) {

            
        });

    });
    
}

firebase.auth().onAuthStateChanged(function(user) {
    var primaryTopic = $(".meta_wrap .temaBTN1");
    var topicName = primaryTopic.children("a").data('name');
    var topicIcon = primaryTopic.find("i");
    var id = "OL18578017";
    if(user){
        user.getIdToken().then(function(token) {
           
            getArticleDetails(token, id, topicName);
            tk = token;

        });
        
    }
});


var smDevices = window.matchMedia("(max-width: 1009px)");
var navHeigth = 62;

$("#dropdown-navbar-toggle").click(function(){
    if ($("#dropdown-navbar").is(':hidden')) {
        $(".dropdown-nav").css("left", "0");
        $("#dropdown-navbar").show(200);
        $("#dropdown-navbar-toggle .icon-close").show(50);
        $("#dropdown-navbar-toggle .navbar-toggler-icon").css("background-image","url(img/icon-close.png)");
        if (smDevices.matches) {
            $("body").css("overflow", "hidden");
        }
        if ($("#dropdown-search").is(':visible') || $("#dropdown-login").is(':visible')) {
            $("#dropdown-search").hide(200);
            $("#dropdown-search-toggle .icon-close").hide(50);
            $("#dropdown-search-toggle img").attr("src","img/icon-search.png");
            $("#dropdown-login").hide(200);
            $("#dropdown-login-toggle .icon-close").hide(50);
            $("#dropdown-login-toggle img").attr("src","img/icon-login.png");
            $("#dropdown-navbar").show(200);
        }
        
    } else if ($("#dropdown-navbar").is(':visible')) {
        $(".dropdown-nav").css("left", "0");
        $("#dropdown-navbar").hide(200);
        $("#dropdown-navbar-toggle .icon-close").hide(50);
        $("#dropdown-navbar-toggle .navbar-toggler-icon").css("background-image","url(img/icon-menu.png)");
        $("body").css("overflow", "auto");
    }
});

$("#dropdown-search-toggle").click(function(){
    if ($("#dropdown-search").is(':hidden')) {
        $(".dropdown-nav").css("left", "unset");
        $("#dropdown-search").show(200);
        $("#dropdown-search-toggle .icon-close").show(50);
        $("#dropdown-search-toggle img").attr("src","img/icon-close.png");
        
        if ($("body").css('overflow') == 'hidden'){
            $("body").css("overflow", "auto");
        }
//        if (smDevices.matches) {
//            $("body").css("overflow", "hidden");
//        }
        if ($("#dropdown-navbar").is(':visible') || $("#dropdown-login").is(':visible')) {
            $("#dropdown-login").hide(200);
            $("#dropdown-login-toggle .icon-close").hide(50);
            $("#dropdown-login-toggle img").attr("src","img/icon-login.png");
            $("#dropdown-navbar").hide(200);
            $("#dropdown-navbar-toggle .icon-close").hide(50);
            $("#dropdown-navbar-toggle .navbar-toggler-icon").css("background-image","url(img/icon-menu.png)");
            $("#dropdown-search").show(200);
        }
        
    } else if ($("#dropdown-search").is(':visible')) {
        $("#dropdown-search").hide(200);
        $("#dropdown-search-toggle .icon-close").hide(50);
        $("#dropdown-search-toggle img").attr("src","img/icon-search.png");
//        $(".dropdown-nav").css("left", "0");
    }
});

$("#dropdown-login-toggle").click(function(){
    if ($("#dropdown-login").is(':hidden')) {
        $(".dropdown-nav").css("left", "unset");
        $("#dropdown-login").show(200);
        $("#dropdown-login-toggle .icon-close").show(50);
        $("#dropdown-login-toggle img").attr("src","img/icon-close.png");
        if ($("body").css('overflow') == 'hidden'){
            $("body").css("overflow", "auto");
        }
//        if (smDevices.matches) {
//            $("body").css("overflow", "hidden");
//        }
        if ($("#dropdown-search").is(':visible') || $("#dropdown-navbar").is(':visible')) {
            $("#dropdown-search").hide(200);
            $("#dropdown-search-toggle .icon-close").hide(50);
            $("#dropdown-search-toggle img").attr("src","img/icon-search.png");
            $("#dropdown-navbar").hide(200);
            $("#dropdown-navbar-toggle .icon-close").hide(50);
            $("#dropdown-navbar-toggle .navbar-toggler-icon").css("background-image","url(img/icon-menu.png)");
            $("#dropdown-login").show(200);
        }
        
    } else if ($("#dropdown-login").is(':visible')) {
        $("#dropdown-login").hide(200);
        $("#dropdown-login-toggle .icon-close").hide(50);
        $("#dropdown-login-toggle img").attr("src","img/icon-login.png");
//        $(".dropdown-nav").css("left", "0");
    }
});

//------------------------------//


$("#dropdown-navbar-toggle, #dropdown-search-toggle, #dropdown-login-toggle").click(function() {
    if (!$(".navbar").hasClass( "fixed-top" ) && (smDevices.matches)) {
        $('html, body').animate({
//            scrollTop: $(".dropdown-nav").offset().top
            scrollTop: $(".dropdown-nav").offset()
        }, 400);
    }
});


//------------------------------//

$(".btn-list-toggle").click(function(){
    if ($(".list-toggle").hasClass("hidden-sm-down")) {
        $(".list-toggle").removeClass("hidden-sm-down");
        $(".list-toggle").addClass("visible-sm-down");
    } else {
        $(".list-toggle").removeClass("visible-sm-down");
        $(".list-toggle").addClass("hidden-sm-down");
    }
});


//----------------------------------------------------//
//----Fix Nav to Top on Scroll----//


$(document).scroll(function () {
    var y = $(this).scrollTop();
    var scrollToNav = 0;
    var scrollToNavMain = 0;
    var scrollToNavSimple = $('.banner-top').outerHeight(true);
    if (!$(".navbar").hasClass("detail-nav")) {
        scrollToNav = scrollToNavMain;
    } else {
        scrollToNav = scrollToNavSimple;
    }
    if (y > scrollToNavSimple) {
        $('#dl-logo-sm').fadeIn(0);
        $('#dl-logo').fadeOut(0);
        
        $('.top-navbar-left').fadeOut(0);
        $('.top-navbar-right').fadeOut(0);
        
        if (!$(".navbar").hasClass("detail-nav")) {
            $( ".navbar" ).addClass( "dl-nav-simple fixed-top" );
        } else {
            $( ".navbar" ).addClass( "fixed-top" );
        }
        // make '62px' a variable if not always exist the Top Banner, do the same for 'scrollToNav' variables
        $("body").css("margin-top", "102px");
        
    } else {
        $('#dl-logo-sm').fadeOut(0);
        $('#dl-logo').fadeIn(0);
        
        $('.top-navbar-left').fadeIn(0);
        $('.top-navbar-right').fadeIn(0);
        
        if (!$(".navbar").hasClass("detail-nav")) {
            $( ".navbar" ).removeClass( "dl-nav-simple fixed-top" );
        } else {
            $( ".navbar" ).removeClass( "fixed-top" );
        }
        $("body").css("margin-top", "0");
        
    }

});


//--------------------------------------------------

//----News Detail----//
//----Scroll Down and Up----//

var lastScrollTop = 0;
$(window).scroll(function(event){
   var st = $(this).scrollTop();
    var showOnlyWhenNeeded = ($('.box-detail-title').innerHeight() + $('#main-nav').innerHeight() + $('.banner-top').outerHeight(true) + navHeigth);
    var mdDevices = window.matchMedia("(min-width: 1010px)");
    
   if (!$(".navbar").hasClass("gallery-nav")) {
       if (st > lastScrollTop){
           $(".dl-nav-simple .navbar-top-sections").fadeOut(0);
           $(".dl-nav-simple .navbar-top-actual-section").fadeIn(0);
           if (($(window).scrollTop() >= showOnlyWhenNeeded) && (mdDevices.matches)) {
                $(".dl-nav-simple .login-search-container").fadeOut(0);
                $(".dl-nav-simple .detail-social-container").fadeIn(0);
                $(".dl-nav-simple .detail-social-container").css("margin-right", "0");
            } else {
                $(".dl-nav-simple .detail-social-container").css("margin-right", "18px");
                if ($(".dl-nav-simple").hasClass("horoscope-nav")) {
                    $(".dl-nav-simple .detail-social-container").css("margin-right", "348px");
                }
            }

       } else {
           $(".dl-nav-simple .navbar-top-sections").fadeIn(0);
           $(".dl-nav-simple .login-search-container").fadeIn(0);
           $(".dl-nav-simple .navbar-top-sections").fadeOut(0);
           $(".dl-nav-simple .detail-social-container").fadeOut(0);
       }
   }
   lastScrollTop = st;
});


//----Fixed Banner when Scrolling----//

$(window).scroll(function () {
    var mdDevices = window.matchMedia("(min-width: 1010px)");
	var fixSidebar = ($('#top-content').innerHeight() + $('#main-nav').innerHeight() + $('.banner-top').innerHeight() + navHeigth - 50);
    if ($('.lasso-sponsored').height() != null) {
        var fixSidebar = ($('#top-content').innerHeight() + $('#main-nav').innerHeight() + $('.banner-top').innerHeight() + navHeigth + $('.lasso-sponsored').height() - 50);
    }
    if ($('#top-content').height() == null) {
        var fixSidebar = ($('#main-nav').innerHeight() + $('.banner-top').innerHeight() + navHeigth - 50);
    }
	var contentHeight = $('#main-content').innerHeight();
	var sidebarHeight = $('#fixed-banner').height();
    var sidebarBottomPos = contentHeight - sidebarHeight; 
    var trigger = $(window).scrollTop() - fixSidebar;

    if ($('#main-content').height() > $('#fixed-banner').height()) {
        if (($(window).scrollTop() >= fixSidebar) && (mdDevices.matches)) {
            $('#fixed-banner').addClass('fixed');
        } else {
            $('#fixed-banner').removeClass('fixed');
        }

        if ((trigger >= sidebarBottomPos) && (mdDevices.matches)) {
            $('#fixed-banner').addClass('bottom');
        } else {
            $('#fixed-banner').removeClass('bottom');
        }
    }
});




//----------------------CAROUSEL----------------------//


$('.carousel').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 1;
    var totalItems = $('.carousel-item').length;
    
    if (idx >= totalItems-(itemsPerSlide-1)) {
        
        var it = itemsPerSlide - (totalItems - idx);
        for (var i=0; i<it; i++) {
            // append slides to end
            if (e.direction=="left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    } 
});


//------show the listed elements as shown in the beginning------//

$(window).resize(function(){
    var mdDevices = window.matchMedia("(min-width: 1010px)");
    if (mdDevices.matches) {
        $('.carousel-item').removeClass('active');
        $('.carousel-item:first-child').addClass('active');
    }
});


//-------------------------------------------//

$(document).ready(function() {
    // TODO: Run function only if elements exist
  var owl = $('.owl-carousel.photos');
  var owl3 = $('.owl-carousel.carousel-col-3');
  var owl4 = $('.owl-carousel.carousel-col-4');
  var owlGallery = $('.owl-carousel.gallery');
  owl.owlCarousel({
    nav: true,
    margin: 0,
    loop: true,
    navText: ["<i class='arrow left'></i>","<i class='arrow right'></i>"],
    items: 1,
    responsive: {
      0: {
        items: 1,
        autoHeight:true
      },
      558: {
        items: 1
      },
      1010: {
        items: 1,
      }
    }
  })

    //--//
  owl3.owlCarousel({
    nav: true,
    stagePadding: 20,
    margin: 0,
    loop: true,
    navText: ["<i class='arrow left'></i>","<i class='arrow right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      558: {
        items: 2
      },
      1010: {
        items: 3,
        touchDrag  : false,
        mouseDrag  : false,
        stagePadding: 0,
      }
    }
  })
    //--//
  owl4.owlCarousel({
    nav: true,
    stagePadding: 20,
    margin: 0,
    loop: true,
    navText: ["<i class='arrow left'></i>","<i class='arrow right'></i>"],
    responsive: {
      0: {
        items: 1
      },
      558: {
        items: 2
      },
      1010: {
        items: 4,
        touchDrag  : false,
        mouseDrag  : false,
        stagePadding: 0,
      }
    }
  })
    //--//
  owlGallery.owlCarousel({
//    nav: true,
    margin: 0,
//    navText: ["<i class='arrow left'></i>","<i class='arrow right'></i>"],
    stagePadding: 0,
    items: 1,
    dots: false,
    onInitialized  : counter,
    onTranslated : counter,
    responsive: {
      0: {
        items: 1
      },
      558: {
        items: 1
      },
      1010: {
        items: 1
      }
    }
  })
    //--//
    $("#see-gallery-again").click(function(){
        owlGallery.trigger('to.owl.carousel', 0);
    });

    $(".owl-prev-container").click(function(){
        owlGallery.trigger('prev.owl.carousel');
    });
    
    $(".owl-next-container").click(function(){
        owlGallery.trigger('next.owl.carousel');
    });
});

function counter(event) {
    var element   = event.target;         // DOM element, in this example .owl-carousel
    var items     = event.item.count;     // Number of items
    var item      = event.item.index + 1;     // Position of the current item
  $('.gallery-counter').html(item+"/"+items)
}


//-------------------------------------------//


$("#see-gallery").click(function () {
   $(".gallery-intro-container").fadeOut(200); 
   $(".gallery-slides-container").fadeIn(200); 

   $(".gallery-counter").fadeIn(200);
   $(".gallery-counter--divisor").fadeIn(200);
});

//---------------//




//-------------------------------------------//

$(".share-box input").click(function () {
   $(".share-box input").select();
});

//Show and Hide Share-Box
$(".share-btn").click(function () {
    var shareBoxTrigger = (this).getAttribute('data-share-target');
    $('#' + shareBoxTrigger).fadeIn(200);
});

$(".share-box-close").click(function () {
    var shareBoxDismiss = (this).getAttribute('data-share-dismiss');
    $('#' + shareBoxDismiss).fadeOut(200);
});

//-------------------------------------------//

//Passing URL to share Buttons
//PRUEBAS
$(".facebook-url").click(function () {
    var insert = $('.content-capsule');
    
//    var insertName = insert.attr("name");
    var insertName = $(this).parent().parent().parent().parent().parent().attr('id');
    
    var insertShareFaceboock = $('.content-capsule'+' [data-network="facebook"]');
    
    var webPageUrl = window.location.href;
    var insert = $(this).parent().parent().parent().parent().parent().attr('id');
    var insertTitle = $('#'+insert+' h3').text();
    var insertParag = $('#'+insert+' p').text();
    
       
       insertShareFaceboock.attr({
            "data-url" : webPageUrl + "#" + insertName,
            "data-title" : insertTitle,
            "data-description" : insertParag,
            "data-message" : insertParag,
            "data-image" : "http://herramienta.dl-cdn24.com/maquetadl/img/img-49.JPG"
        });
    alert(insertParag);
});

//-------------------------------------------//

//PRUEBAS
$(".googleplus-url").click(function () {
    var insert = $('.content-capsule');
    
//    var insertName = insert.attr("name");
    var insertName = $(this).parent().parent().parent().parent().parent().attr('id');
    
    var insertShareGooglePLus = $('.content-capsule'+' [data-network="googleplus"]');
    
    var webPageUrl = window.location.href;
    var insert = $(this).parent().parent().parent().parent().parent().attr('id');
    var insertTitle = $('#'+insert+' h3').text();
    var insertParag = $('#'+insert+' p').text();
    
       
       insertShareGooglePLus.attr({
            "data-url" : webPageUrl + "#" + insertName,
            "data-title" : insertTitle,
            "data-description" : insertParag,
            "data-message" : insertParag,
            "data-image" : "http://herramienta.dl-cdn24.com/maquetadl/img/img-49.JPG"
        });
    
    alert(insertParag);
});

//PRUEBAS
//-------------------------------------------//

(function(document) {
   var shareButtons = document.querySelectorAll(".st-custom-button[data-network]");
   for(var i = 0; i < shareButtons.length; i++) {
      var shareButton = shareButtons[i];
      
      shareButton.addEventListener("click", function(e) {
         var elm = e.target;
         var network = elm.dataset.network;
         
         console.log("share click: " + network);
      });
   }
})(document);


//------------------------------------------//
//-----------Menu Bloque Videos-------------//

$('.video-menu-toggle').on('blur', function() {
    $('.video-toggle').fadeOut('medium');
});
$('.video-menu-toggle').on('focus', function() {
    $(this).siblings('.video-toggle').show();
});
$('.video-toggle').hide();

//------------------------------------------//
//-----------Menu Bloque Videos-------------//

$(".nav-link.video").click(function () {
    var videoContentTrigger = (this).getAttribute('aria-controls');
    $('.tab-pane.video').removeClass("active");
    $('.content-' + videoContentTrigger).addClass("active");
});

//-----------------------------------------//

$(".ajax-trigger").click(function(){
//    alert("hola");
    var videosContentURL = (this).getAttribute('ajax-target');
    $('.tab-pane.active').hide();
    $(".videos-tab .nav-link").removeClass('active');
    $('.video-menu-toggle').addClass('active');
    $('.ajax-content').show();
    $(".ajax-content").load(videosContentURL + " .box-detail-title");
});


 
$(".videos-tab").click(function(){
//    if () {
        $('.tab-pane.active').show();
        $('.ajax-content').hide();
//    }
});

if( $(".related-box div div").length > 0 ) {
    $(".related-box").show();
} else {
    $(".related-box").hide();
}


//------------------------------------------//

$(".js--sticky-ad").stick_in_parent({offset_top: 60});






