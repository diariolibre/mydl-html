const BASE_URI = 'https://us-central1-dominican-got-talent-2b6c4.cloudfunctions.net/api/v1';
const MI_DL = 'mi-dl.html';
const BASE_SITE = 'https://herramienta.diariolibre.com/DiarioLibreTest3';
var user = null;

function getAvatar(){
    var avatars = ["01","02","03","04","05","06","07","08"];
    var rand = Math.floor(Math.random() * avatars.length);
    
    return 'r/img/miDLavatar-'+avatars[rand]+'.png';
}

function showAlert(errorMsg, field) {
    $(".sbr_input").removeClass("error");
    $(".alert-box.error span").text(errorMsg);
    if (field == "email") {
        $("#auth-email").addClass("error");
    } else if (field == "password") {
        $("#auth-pass").addClass("error");
        $("#auth-pass2").addClass("error");
    } else {
        $(".sbr_input").addClass("error");
    }
    $(".alert-box.error").show(100);
}

var box = {
    resize: function(){
        var w = $('body').width();
        if (w <= 845) {
            $('.sbr_tabs .t1').html('Acceder');
            $('.sbr_tabs .t2').html('Registrarme');
        }else{
            $('.sbr_tabs .t1').html('Iniciar sesión');
            $('.sbr_tabs .t2').html('Crear cuenta');
        }
    },
    tab: function(a){
        a = $(a);
       
        $('.sbr_tabs li').removeClass('active');
        a.addClass('active');

        if (a.hasClass('t2')) {
            $('.sbrSignupCont').show();
            $('.sbrLoginCont').hide();
        }else{
            $('.sbrSignupCont').hide();
            $('.sbrLoginCont').show();
        }
        
        var tabClass = a[0].classList[0];

        $("#userContent").children().removeClass("active");
        $("#"+tabClass).addClass("active");
        
    }
}

box.resize();
$(window).resize(box.resize);

$(".logout_btn").on("click", function(){
    firebase.auth().signOut().then(function() {
        window.location.replace("login.html");
    }, function(error) {
        console.log(error);
    });
});

$(".sidebar_cont").on("click", ".sbc_logout_btn", function(){
    firebase.auth().signOut().then(function() {
        window.location.replace("login.html");
    }, function(error) {
        console.log(error);
    });
});

firebase.auth().onAuthStateChanged(function(user) {
    user = user;
    if(user !== null && user.emailVerified === false){
        $(".confirmationNotification").css("display","flex").children("span").html('Por favor verificar tu cuenta de correo <b>'+user.email+'</b> para una mejor experiencia. <a href="#" class="validar-cuenta"><strong>Reenviar correo de validación</strong></a>');
        $(".confirmationNotification.fwp").children("i").html("error");
    }
});



function sendValidation(){

    firebase.auth().currentUser.sendEmailVerification();
    $(".confirmationNotification").css("display","flex").children("span").html('Se ha enviado un correo de verificación al correo <strong>'+firebase.auth().currentUser.email+ '</strong>, por favor confirmar su cuenta.</a>');
    $(".confirmationNotification").children("i").html("check_circle");

}

$(".confirmationNotification").on("click",".validar-cuenta", function(e){
    sendValidation();
    e.preventDefault();
});


