var bcProfile = '';
var bCClient = '';
var oneSignalId = '';
var bCEmail ='';
  
  // Is BlueConic loaded?
  if (typeof window.blueConicClient !== 'undefined' &&
      typeof window.blueConicClient.event !== 'undefined' &&
      typeof window.blueConicClient.event.subscribe !== 'undefined') {
      profile = blueConicClient.profile.getProfile();
      bCClient = blueConicClient;
      var properties = ['DL_Name', 'bc_dev_login_address','date_subscription_midl'];
      profile.loadValues(properties, this, function() {
        // Values have been loaded, safe to get now
        var DL_Name = profile.getValue('DL_Name');
        var bc_dev_login_address = profile.getValue('bc_dev_login_address');
        console.log('DL_Name:', DL_Name);
        console.log('bc_dev_login_address:', bc_dev_login_address);
        console.log('date_subscription_midl:', profile.getValue('date_subscription_midl'));
        bCEmail = bc_dev_login_address;
      });
  } else {
    // Not yet loaded; wait for the "onBlueConicLoaded" event
    window.addEventListener('onBlueConicLoaded', function () {
      // BlueConic is loaded, now we can do API things
      profile = blueConicClient.profile.getProfile();
      bCClient = blueConicClient;
      var properties = ['DL_Name', 'bc_dev_login_address','date_subscription_midl'];
      profile.loadValues(properties, this, function() {
        // Values have been loaded, safe to get now
        var DL_Name = profile.getValue('DL_Name');
        var bc_dev_login_address = profile.getValue('bc_dev_login_address');
        console.log('DL_Name:', DL_Name);
        console.log('bc_dev_login_address:', bc_dev_login_address);
        console.log('date_subscription_midl:', profile.getValue('date_subscription_midl'));
        bCEmail = bc_dev_login_address;
      });
    }, false);
}

$(function(){

    var tmp_avatar = getAvatar();

    function loadUserData(token){
        var settings = {
            "url": BASE_URI+"/accounts?",
            "method": "GET",
            "timeout": 0,
            "headers": {
            "Authorization": token
            },
        };

        $.ajax(settings).done(function (response) {
            console.log(response);
            console.table(response.data);
        
            if(typeof response.data !== 'undefined'){

                typeof response.data.name !== null ? $("#auth-name").val(response.data.name) : $("#auth-surname").val(firebase.auth().currentUser.displayName);
                typeof response.data.last_name !== null ? $("#auth-surname").val(response.data.last_name) : $("#auth-surname").val('');

                firebase.auth().currentUser.photoURL !== null ? 
                    $('.data-user-welcome img').attr("src",firebase.auth().currentUser.photoURL).show() : 
                    $('.data-user-welcome img').attr("src",tmp_avatar).show();
                
                if(response.data.hasOwnProperty('is_complete') && response.data.is_complete == true && firebase.auth().currentUser.email != null){
                    $(".sbr_btnsCont.confirm").html(overlayForm());
                }
            }
            typeof firebase.auth().currentUser.email !== null ?
                    $("#auth-email").val(firebase.auth().currentUser.email):
                    $("#auth-email").val("");
            $(".sbr_btnsCont.confirm").css({"display":"flex","justify-content":"center","margin-top":"100px"});
        });
    }

    firebase.auth().onAuthStateChanged(function(user) {
        console.log("user?",user);
        //return ;
        if (user) {

            user.getIdToken().then(function(data) {
                loadUserData(data);
            });
            
            var authName = firebase.auth().currentUser.displayName;
            var authPhoto = firebase.auth().currentUser.photoURL;

            authName == null ? $(".data-user-welcome span").text("¡Hola!") : $(".data-user-welcome span").text("¡Hola " + authName + "!");
            
            authPhoto !== null ? $('.data-user-welcome img').attr("src",authPhoto).show() : $('.data-user-welcome img').attr("src",tmp_avatar).show();
        
            $(".data-user-welcome").show();
            
        } else {
            window.location.replace("login.html");
        }
    });

    function overlayForm(){
        //$(".signupBox .sb_right").addClass("overlayFormFlex");
        //$(".sbr_title.fwp").html("Usted ya se encuentra logueado");
        var html = '<div class="overlayForm">';
                html +='<p>Sus datos se encuentran completos, por favor continúe hacia su perfil o introduzca otras credenciales.</p>';
                html += '<a href="#" class="sbr_btn email logout">Entrar con otras credenciales</a>';
                html += '<a href="mi-dl.html" class="sbr_btn email">Ir a MiDL</a>';
            html += '</div>';

        return html;
    }

    $(".sbr_btnsCont.confirm").on("click",".overlayForm .logout", function(){
        firebase.auth().signOut().then(function() {
            location.reload(true);
          }).catch(function(error) {
            // An error happened.
          })
    });
    
    
    function ValidateForm() {
        
        var authEmail = $("#auth-email").val();
        var authName = $("#auth-name").val();
        var authSurname = $("#auth-surname").val();
        
        var authEmailService = firebase.auth().currentUser.email;
        var authPhoto = firebase.auth().currentUser.photoURL;

        $.validator.addMethod("noSpace", function (value, element) {
            return this.optional(element) || /.*\S+.*/.test(value);
        }, "No se permite introducir espacios en blanco");
        
        var validator = $("#auth").validate({
            rules: {
                auth_email: {
                required: true,
                email: true,
                noSpace: true
                },
                auth_name:{
                    noSpace: true,
                },
                auth_surname:{
                    noSpace: true,
                }
            }

        });
        
        if ((authEmail != "") && (authName != "") && (authSurname != "")) {
            
            if (validator.form()) {
                $(".sbr_input").removeClass("error-field");
                $(".alert-box").hide(100);
                return true;  
            } else {
                $(".alert-box").show(100);
                return false;
            }              
            
        }

    }
        
    function showError(error) {
        $(".alert-box span").text(error);
        $(".sbr_input").addClass("error");
        $(".alert-box").show(100);
    }    
        
    function register() {
        
        firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
            // Send token to your backend via HTTPS
            sendRegisterWithToken(idToken);

        }).catch(function(error) {
            // Handle error
            //Enviar al login
            console.log(error);
            window.location.replace("login.html");
        });
        
        
    }
    
    
    function sendRegisterWithToken(token) {
        
        var authEmail = $("#auth-email").val();
        var authName = $("#auth-name").val();
        var authSurname = $("#auth-surname").val();
        var bc_profile = profile;
        var authPhoto = "";

        if(firebase.auth().currentUser.photoURL !== null){
            authPhoto = firebase.auth().currentUser.photoURL;
        }else{
            authPhoto = tmp_avatar;
        }

        var is_email_bc = false;

        /*if($.trim(bc_profile.getValues('bc_dev_login_address')) == $.trim(authEmail)){
            is_email_bc = true
        }*/

        
        var validate = ValidateForm();
        if(validate == true) {
            var settings = {
                    "url": BASE_URI+"/accounts",
                    "method": "POST",
                    "timeout": 0,
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Authorization": token
                    },
                    "data": {
                        "name": authName,
                        "last_name": authSurname,
                        "email": authEmail,
                        "avatar": authPhoto,
                        "is_complete": true,
                        //"is_email_bc": is_email_bc
                    }
            };

            $.ajax(settings).done(function (response) {
                              
                var user = firebase.auth().currentUser;
                var actual_email = firebase.auth().currentUser.email;

                user.updateProfile({
                    photoURL: authPhoto,
                    displayName: authName+ ' '+authSurname
                }).then(function() {

                    console.log(user);

                    user.updateEmail(authEmail).then(function() {
                        console.log("authEmail",authEmail);
                        window.location.replace("perfil-informacion.html");
                        
                    }).catch(function(error) {
                        
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        console.log(error);
                        var values = showErrorMsg(errorCode);
                        var first = values[0];
                        var second = values[1];
                        
                        //showAlertLogin(first, second);
                        console.log(first);
                        console.log(second);
                        $(".sbr_btn").html("Continuar").removeAttr("disabled");
                        $(".alert-box").show(100).children("span").html(first);
                        
                        
                    });

                    bc_profile.setValue("bc_dev_login_address", authEmail);
                    bc_profile.setValue("DL_Name", authName);
                    bc_profile.setValue("DL_Last_Name", authSurname);
                    bc_profile.setValue("date_update_midl", new Date());                    
                    
                    bCClient.profile.updateProfile();
                    
                    
                }).catch(function(error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    console.log(error);
                    var values = showErrorMsg(errorCode);
                    var first = values[0];
                    var second = values[1];
                    
                    //showAlertLogin(first, second);
                    console.log(first);
                    console.log(second);
                    $(".sbr_btn").html("Continuar").removeAttr("disabled");
                    $(".alert-box").show(100).children("span").html(first);
                    
                });                

            }) .fail(function(error) {
                console.log(error);
                var errorMsg = error.responseJSON.message;
                showError(errorMsg);
                //$(".alert-box").show(100);
                $(".alert-box").show(100).children("span").html(first);
                $(".sbr_btn").html("Continuar").removeAttr("disabled");
            });
        } else {
            return false;
        }
    }

    $("#auth-email").keydown(function (e) {
        if (e.keyCode == 32) { 
          $(this).val($(this).val() + "");
          return false; 
        }
   });

    $("#auth-email").on('keyup change',function(){
        var value = $(this).val();
        var domain = value.substr(value.indexOf("@") + 1);
        var tld = domain.split(".").pop();

        if(tld.length > 1){
            $(".alert-box.form-confirm").hide();
        }
    });

    $(".sbr_btn").on("click", function(){
                            
        $.validator.methods.email = function( value, element ) {
            return this.optional( element ) || /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}/.test( value );
        }
        $.validator.addMethod("noSpace", function (value, element) {
            return this.optional(element) || /.*\S+.*/.test(value);
        }, "No se permite introducir espacios en blanco");

        $("#auth").validate({
            rules: {
                auth_email: {
                    email: true,
                    noSpace: true
                },
                auth_name:{
                    noSpace: true,
                },
                auth_surname:{
                    noSpace: true,
                }
                
            },
            messages:{
                auth_email:{
                    auth_email: "Introduzca un correo electrónico válido",
                    required: "El campo Correo electrónico es requerido"
                },
                auth_name:{
                    required: "El campo Nombre es requerido"
                },
                auth_surname:{
                    required: "El campo Apellido es requerido",
                }
            },
            errorPlacement: function(error, element) {
                $(".alert-box.form-confirm span").html("");
                error.appendTo($(".alert-box.form-confirm").show().children("span"));   
            },
            submitHandler: function(form) {
                $(".sbr_btn").html("<img src='r/img/loading.gif'>").attr("disabled", 'disabled');
                register();
                
            }
        });
    });
        
});